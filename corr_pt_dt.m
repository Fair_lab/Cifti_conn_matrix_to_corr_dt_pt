function corr_pt_dt(seed_conc_file, dt_series_file, ix, output_name, ...
    mask, make_dscalars, wb_command, extraction_type, num_components, ...
    fisherz)

%% corr_pt_dt
% Oscar Miranda-Dominguez, Oct. 13, 2016

% This function calculates the correlation between a given set of ROIs from
% a parcellated time series to all the timecourses from a dense time series
%
% The output is a dtseries file
%
% Mandatory inputs:
%   pt_series_file,
%       example: pt_series_file='/group_shares/FAIR_HCP/HCP/processed/ADHD-HumanYouth-OHSU/10050-1/20120605-SIEMENS-Nagel_K-Study/HCP_prerelease_FNL_0_1/MNINonLinear/Results/10050-1_FNL_preproc_Gordon_subcortical.ptseries.nii';
%
%   dt_series_file,
%       example: dt_series_file='/group_shares/FAIR_HCP/HCP/processed/ADHD-HumanYouth-OHSU/10050-1/20120605-SIEMENS-Nagel_K-Study/HCP_prerelease_FNL_0_1/MNINonLinear/Results/10050-1_FNL_preproc_Atlas.dtseries.nii';
%
% Optional inputs:
%
%   ix, indices of the ROIS in the dt_series. This is optional. If not
%   provided, correlation will be calculated using as seed all the ROIs
%   from the ptseries. If provided, It needs to be a vector,
%       example: ix=[1 2];
%
%   output_file, Optional, If provided, it should have an existing path and filename
%   (without the extension dtseries.nii). If not provided, is going to take
%   the path from the ptseries and the filename will be
%   corr_pt_dt.dtseries.nii
%
%  mask, Optional. If provided, removes those frames from the timeseries.
%  One means keep, 0 means delete

%% Robert Hermosillo, Oct 5, 2017
%  added an option to add save the correlations as a dscalar rather than a
%  dtseries. Add addtional variable to make_dscalars.
%% Robert Hermosillo, Feb 20, 2018
% added a funtion that checks for .txt extentsion is ROI seed is already
% built.
%% Robert Hermosillo, Jul 19, 2019
% added a wb_command to be passed in from external wrapper.
%% Robert Hermosillo, Sept 4, 2019
% added a loop to run a vector of indexs(ix)
% added an option for fisher-Z transforation
% added pca extraction from the time series to generate parcellation.

%% Load settings
% This code lives originally here: /group_shares/PSYCH/code/development/utilities/corr_pt_dt
% remeber to add the containing folder to your path: addpath(genpath('/group_shares/PSYCH/code/development/utilities/corr_pt_dt'));
%%
% if ~isdeployed
%     addpath('/mnt/max/shared/code/internal/utilities/corr_pt_dt/support_files');
% end
%path_to_dscalar_template='/mnt/max/shared/data/study/ADHD/HCP/processed/ADHD_NoFMNoT2/10499-1/20150425-SIEMENS_TrioTim-Nagel_K_Study/HCP_release_20161027/10499-1/MNINonLinear/Results/10499-1_FNL_preproc_Atlas_SMOOTHED_2.55.dtseries.nii_all_frames_at_FD_none_ROI342.dscalar.nii.dtseries.nii';
path_to_dscalar_template=dt_series_file;
%settings=settings_corr_pt_dt;%
%% Adding paths for this function
% np=size(settings.path,2);
% for i=1:np
%     addpath(genpath(settings.path{i}));
% end
%
% wb_command=settings.path_wb_c; %path to wb_command

%% Make default name for output file, if not provided

ROI_names = strsplit(output_name,' '); % fix outputnames of vectors

if length(ROI_names)>1
    for k=1:length(ROI_names)-1
        if k ==1
            updated_ROI_name = [char(ROI_names(k)) char(ROI_names(k+1))];
        else
            updated_ROI_name = [updated_ROI_name char(ROI_names(k+1))];
        end
    end
    
    output_name = updated_ROI_name;
    [~,potentialylongfilename,~] = fileparts(updated_ROI_name);
    
    if length(potentialylongfilename) > 100
        disp('Filename will be very long based on number of ROIs. Shortening that for you.')
        long_file_name_parts = strsplit(potentialylongfilename,'ROI');
        output_name = [char(long_file_name_parts{1}) 'many_ROIs.dtseries.nii'];
    else
    end
else
end

if exist('output_name','var') == 0
    output_name=[];
end
if isempty(output_name)
    [pathstr_dt,name_dt,ext_dt] = fileparts(dt_series_file);
    % pathstr_out=pathstr_pt;
    output_name=[pathstr_dt filesep 'corr_pt_dt'];
else
end

% if or(nargin<4,isempty(output_file))
% % [pathstr_pt,name_pt,ext_pt] = fileparts(pt_series_file);
% [pathstr_dt,name_dt,ext_dt] = fileparts(dt_series_file);
% % pathstr_out=pathstr_pt;
% output_file=[pathstr_dt filesep 'corr_pt_dt'];
% end

%% Read dtseries
if isdeployed ==0
    disp(['wb_command is ' wb_command]);
    cii=ciftiopen(dt_series_file,wb_command);
    %cii=cifti_open(dt_series_file,wb_command);
    newcii=cii;
    dt_series=double(newcii.cdata);
else
    % ft_read_cifti_mod(dt_series_file) 
    dt_series_struct=ft_read_cifti_mod(dt_series_file);
    dt_series=dt_series_struct.data;
end


%% Make seed  by either reading the ptseries, txt, csv, or applying a label to dtseries
if ischar(seed_conc_file)==1
    seed_conc_file=cellstr(seed_conc_file);
else
end
i=1;

for j=1:size(ix,2)

    if isnumeric(seed_conc_file) == 1
        seed_series_final=seed_conc_file; % assume that a vector (1 row by x timepoints was provided).
        
    elseif strcmp(seed_conc_file{i}(end-3:end),'.txt') == 1

        disp('reading seed from txt file.')
        seedfile=importdata(seed_conc_file{i});
        [lengthofseedfile, widthofseedfile] = size(seedfile);
        if lengthofseedfile >= widthofseedfile
            NOTE = ['Subject ' num2str(i) ' needs to have times series data as a row, instead of a column']
            return
        else
            seed_series_final=seedfile;
        end
        
    elseif strcmp(seed_conc_file{i}(end-3:end),'.csv') == 1

        disp('reading seed from csv file.')
        seedfile=importdata(seed_conc_file{i});
        seed_series_final=seedfile;
        
    elseif strcmp(seed_conc_file{i}(end-12:end),'.ptseries.nii') == 1 || strcmp(seed_conc_file{i}(end-12:end),'.dtseries.nii') == 1 %read ptseries file - Greg changed the int from 11 to 12 on 2019-08-30
        
        disp('reading seed from ptseries or dtseries file.')
        %pt_series_struct=ft_read_cifti_mod(char(seed_conc_file{i})); % Greg added the char() call 2019-08-30
        %pt_series = pt_series_struct.data;
        if isdeployed ==0 
            cii=ciftiopen(char(seed_conc_file{i}),wb_command); % Greg added the char() call 2019-08-30
            %cii=cifti_open(char(seed_conc_file{i}),wb_command); % Greg added the char() call 2019-08-30
            newcii=cii;
            pt_series=double(newcii.cdata);
        else
            cii=ft_read_cifti_mod(char(seed_conc_file{i}));
            newcii=cii;
            pt_series=double(newcii.data);
        end
        
        % Count the total rois in this parcellation
        N_rois=size(pt_series, 1);
        
        % Error-check to make sure ix is a vector and less than or equal to 
        % N_rois
        if isempty(ix)
            ix=1:n_rois;
        else
            if min(size(ix,1),size(ix,2))>2
                error('ix needs to be a vector or integer')
            end
            if sum(ix>N_rois)>0
                error(['Largest ROI ix must be less than or equal to ' num2str(N_rois)])
            end
        end

        % Extract the pt_series you like the correlation to be calculated
        seed_series_final=pt_series(ix(j),:);
        
        
    elseif strcmp(seed_conc_file{i}(end-11:end),'.dscalar.nii') == 1 %read from dscalar file
        disp('reading seed from dscalar ROI file.')
        
        if isdeployed ==0
            ROI_file = ciftiopen(seed_conc_file{i},wb_command);
            %ROI_file = cifti_open(seed_conc_file{i},wb_command);
            ROI_data = ROI_file.cdata;
        else
            ROI_file = ft_read_cifti_mod(seed_conc_file{i});
            ROI_data = ROI_file.data;
        end
        ROI_file_split = split(dscalar_roifile, filesep);
        ROI_filename = ROI_file_split(end);
        %ROI_file = ft_read_cifti_mod(seed_conc_file{i});
        
        dtseries_sub_ROI = dt_series(ROI_data==1,:);
        
        switch(extraction_type)
            case('pca')
                [components,~,~,~,varexplained] = pca(dtseries_sub_ROI','NumComponents',num_components);
                pca_dtseries_ROI = dtseries_sub_ROI*0;
                for curr_component = 1:num_components
                    pca_dtseries_ROI = pca_dtseries_ROI + (dtseries_sub_ROI.*components(:,curr_component)).*(varexplained(curr_component)/100);
                end
                seed_series_final = mean(pca_dtseries_ROI,1);
            case('mean')
                seed_series_final = mean(dtseries_sub_ROI,1);
            otherwise
                disp('Extraction Type from time series must either be "pca" or "mean".')
        end
        
        
    else
        disp('Input seed must be a .txt, .csv, ptseries with corresponding index, or a dscalar with an ROI.')
        
    end
    
    %% Get prepared for motion censoring
    if exist('mask','var') == 0
        n_frames=size(dt_series,2);
        mask=ones(n_frames,1)==1;
    else % use provided mask
    end
    
    %% calculate correlations
    
    R(:,j)=corr(seed_series_final(:,mask)',dt_series(:,mask)');
    
    
    %% Check for Nans in data.
    display(strcat('Number of NaNs found in series is: ',num2str(sum(isnan(R(j))))))
    if sum(isnan(R(j))) ~= 0 % Added by Greg 2019-08-30
        disp('replacing NaNs with randomly generated numbers')
        R(j(isnan(R(j)),1)) = randn(sum(isnan(R(j))),1)*std(R(j(isnan(R(j)))==0,1))+mean(R(j(isnan(R(j)))==0,1));
    end
    if exist('ROI_data', 'var') % Added by Greg 2019-08-30
        disp('replacing seed region with randomly generated numbers')
        R(j(ROI_data==1,1)) = randn(sum(ROI_data==1),1)*std(R(j(ROI_data==0,1)))+mean(R(j(ROI_data==0,1)));
    end
    
end

if exist('fisherz','var') == 1 && fisherz == 1
    % This variable name is kept as R even though it's Fisher-z transformed.
    R = atanh(R); 
else
end

%% Save data as cifti
if exist('make_dscalars','var') && make_dscalars==1

    % Save as dscalar
    
    if isdeployed == 0
        
        cii_dscalar=ciftiopen(path_to_dscalar_template, wb_command);
        %cii_dscalar=ft_read_cifti_mod(path_to_dscalar_template);
        
        % cii_dscalar;
        newcii=cii_dscalar;
        %newcii.cdata=R;
        newcii.cdata=R;
        
        % Making your cifti
        % ciftisave(newcii,output_file,path_wb_c); 
        % modified from save to include reset-scalars flag.
        tic
        % newcii
        save(newcii, [output_name '.gii'], 'ExternalFileBinary')
        toc
        
        tic
        
        if strcmp(output_name(end-10:end),'dscalar.nii') ==1
            unix([wb_command ' -cifti-convert -from-gifti-ext ' output_name '.gii ' output_name ' -reset-scalars' ]);
        else
            unix([wb_command ' -cifti-convert -from-gifti-ext ' output_name '.gii ' [output_name '.dscalar.nii'] ' -reset-scalars' ]);
            
        end
        toc
        
        unix([' /bin/rm ' output_name '.gii ' output_name '.dat ']);
        disp(['Done, file ' output_name '  saved']);
        
    else

        % Read in template that's actually a dscalar
        mfilename('fullpath')
        pwd()
        cii_dscalar=ft_read_cifti_mod('/home/feczk001/day00096/ili_container/Cifti_conn_matrix_to_corr_dt_pt/example.dscalar.nii');
        
        newcii=cii_dscalar;
        newcii.data=R;

        disp("Deployed dscalar ...")
        disp("Saving to")
        disp(output_name)

        ft_write_cifti_mod(output_name, newcii);
       
    end
    
    
    
else %save as dtseries
    if isdeployed == 0
        % Making your cifti
        cii_dtseries=ciftiopen(dt_series_file,wb_command);
        cii_dtseries.cdata=R;
        %newcii.cdata=R;
        %output_name=[output_name '.dtseries.nii'];
        %output_file=[output_file];
        ciftisave(cii_dtseries,output_name,wb_command); 
        disp(['Done, file ' output_name '  saved']);
    else
        cii_dtseries=ft_read_cifti_mod(dt_series_file);
        cii_dtseries.data=R;
        
        ft_write_cifti_mod(output_name,cii_dtseries);
        disp(['Done, file ' output_name '  saved']);
        
    end
    
end

if isnumeric(seed_conc_file) == 1
    
else
    if strcmp(seed_conc_file{i}(end-11:end),'.dscalar.nii') == 1
        
        disp('Saving time series file...')

        ts_file_output = char(strcat(output_directory, filesep, ...
                                        ROI_filename, '_', ...
                                        extraction_type, ...
                                        '_timeseries.csv'));

        dlmwrite(ts_file_output, new_dtseries_ROI);
    else
    end
end

%% Validation
% temp=randi(3);
% ix1=ix(temp);
% ix2=randi(91282);
% R2=corr(pt_series(ix1,:)',dt_series(ix2,:)');
% [R2 R(temp,ix2)]
