%addpath('/mnt/max/shared/code/external/utilities/gifti-1.6/gifti-1.6')
%addpath('/mnt/max/shared/code/external/utilities/xmltree-2.0')

%Example below
%mcc -v -m -d /mnt/max/shared/code/internal/utilities/corr_pt_dt -o cifti_conn_matrix_to_corr_pt_dt_exaversion /mnt/max/shared/code/internal/utilities/corr_pt_dt/cifti_conn_matrix_to_corr_pt_dt_exaversion.m -a /mnt/max/shared/code/external/utilities/Matlab_CIFTI/ -a /mnt/max/shared/code/internal/utilities/hcp_comm_det_damien/isthisanoutlier.m -a /mnt/max/shared/code/internal/utilities/corr_pt_dt/corr_pt_dt_exaversion.m

system('/mnt/max/software/MATLAB/R2019a/bin/mcc -v -m -d /mnt/max/shared/code/internal/utilities/corr_pt_dt -o corr_pt_dt /mnt/max/shared/code/internal/utilities/corr_pt_dt/corr_pt_dt.m -a /mnt/max/shared/code/external/utilities/Matlab_CIFTI/ -a /mnt/max/shared/code/internal/utilities/hcp_comm_det_damien/isthisanoutlier.m -a /mnt/max/shared/code/external/utilities/gifti-1.6/gifti-1.6/ -a /mnt/max/shared/code/external/utilities/xmltree-2.0/')