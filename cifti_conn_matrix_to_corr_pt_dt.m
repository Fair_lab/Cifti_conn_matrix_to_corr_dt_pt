function  cifti_conn_matrix_to_corr_pt_dt(dtseries_conc_file, ...
    ptseries_or_label_conc_file, motion_file, FD_threshold, TR, ix, ...
    minutes_limit, smoothing_kernel, ...
    left_surface_file, right_surface_file, ...
    wb_command, extraction_type, num_components, fisherz, ...
    remove_outliers, output_dir)

% R. Hermosillo 2/19/2018
% Modified to accept index as an input arguement.
% Check for arguments as strings and converts them to numbers
% (in case a compiled version is used).

% R. Hermosillo 12/12/2023 - update
% updated documentation for additional input arguments.

% dtseries_conc_file = (char) path to the dense timeseries parcellated timeseries conc file %(i.e. text file with paths to each dtseries)
% ptseries_conc_file = (char) path to the parcellated timeseries parcellated timeseries conc file %(i.e. text file with paths to each ptseries)
% motion_file = (char) conc path to the file that points to DCANBOLDProc (i.e.Power2014_FD_only.mat) motion mat files for each dt or ptseries (note: if no motion file to be used, type 'none')
% FD_threshold = (numeric) specify motion threshold (e.g. 0.2)
% TR = (numeric) TR of your data
% ix = (numeric) index of your ROI in the parcelation (i.e. here ROIS 342=ACCUMBENS_LEFT and 162=162_R_Default 341=Amygdala-L, 350=Amygdala-R
% minutes_limit = (numeric) specify the number of minutes to be used to generate the correlation matrix
% smoothing_kernel = (numeric) specify smoothing kernel (note: if no smoothing file to be used, type 'none')
% L_surfacefile = (char) path to a conc of L midthickness file
% R_surface_file = (char) path to a conc of R midthickness file
% wb_command = (char) path to your install version of workbench. Get it from here if you don't have it: https://www.humanconnectome.org/software/connectome-workbench
% extraction_type = (char) For use with feature extraction when in your ptseries. Acceptable options are 'pca' or 'mean'.  PCA is uncommon and not validated.  You probably want mmean'.  Mean will use the mean of the parcel.  PCA will take the components from the dtseries and to create the parcel timeseries.
% num_components = (numeric) The number of componets to use in the pca.  If you're not doing pca (previous input), this variable is not used, so just set it to 0.
% fischerz = (numeric) Determine whether you want your data to fisher Z transformed. If this is set to 1, it will transform the data. if set to 0, your data  will be in Pearon's r.
% remove outliers = (numeric) Set to 1 if you want to remove frames that are outliers (as determined by the standard deivation), and set to 0 if you don't want to remove outliers.  Note, the outlier calculation is performed on the surviing frames after motion censoring.  It is recommended that you  remove outliers.
% output_dir = (char) the path to your desired output directory.

% example:
% cifti_conn_matrix('/my/path_to/group_dtseries.conc','dtseries','/my/path_to/group_motion.conc',0.2, 2.5, 5, 2.55,'/my/path_to/group_right_midthickness_surfaces.conc','/mnt/max/shared/code/internal/utilities/hcp_comm_det_damien/ADHD_DVARS_group_right_midthickness_surfaces.conc','/path/to/wb_command','mean',0,0,1,'/path/to/output_directory/');

% example without smoothing:
% cifti_conn_matrix('/my/path_to/group_dtseries.conc','dtseries','/my/path_to/group_motion.conc', 0.2, 2.5, 5, 'none', 'none', 'none,''/path/to/wb_command','mean',0,0,1,'/path/to/output_directory/');

disp('Printing input variables...')

dtseries_conc_file
ptseries_or_label_conc_file
motion_file
FD_threshold
TR
ix
minutes_limit
smoothing_kernel
left_surface_file
right_surface_file
wb_command
extraction_type
num_components
fisherz
remove_outliers
output_dir

% Define the seed ROIs
% selecting here ROIS 342) ACCUMBENS_LEFT and 
% 162=162_R_Default 341=Amygdala-L, 350=Amygdala-R
% ix=[350]; 

% Add dependencies to path (added by Greg 2019-08-30)
% if ~isdeployed
%     addpath(genpath('/mnt/max/shared/code/external/utilities/Matlab_CIFTI/'));
%     addpath(genpath('/mnt/max/shared/code/external/utilities/gifti-1.6/gifti-1.6/'));
% end

% convert number variables to cnumbers (only within compiled code);
if isnumeric(ix)==1
else
    % don't use str2double - doesn't work with lists
    ix=str2num(ix);
end

if isnumeric(FD_threshold)==1
else
    FD_threshold=str2double(FD_threshold);
end

if isnumeric(TR)==1
else
    TR=str2double(TR);
end

% Check to make sure that minutes limit is a number (unless you've set it
% to 'none')
if strcmp(minutes_limit,'none')==1 || strcmp(minutes_limit,'None')==1 || strcmp(minutes_limit,'NONE')==1
    minutes_limit='none';
elseif isnumeric(minutes_limit)==1
    disp('minutes limit is passed in as numeric.')
else
    disp('minutes limit is passed in as string. converting to numeric')
    minutes_limit=str2double(minutes_limit);
end

% added to ensure randomness of frame sampling during parellization -RH 05/30/2023
reset(RandStream.getGlobalStream,sum(100*clock)); 

% if make scalars =1, then the output file will be row of correlation 
% values as a dscalar.  If 0, then correlations will be saved as a 
% dtseries.
%
% if make_dscalars==1
%     outsuffix = 'dscalar.nii';
% else
%     outsuffix = 'dtseries.nii';
%     make_dscalars=0; % changed from [] to 0 by Greg 2019-08-30
% end

series='dtseries'; %DO NOT change file extention here.

if exist('remove_outliers','var') == 1
    if isnumeric(remove_outliers)==1
    else
        remove_outliers = str2num(remove_outliers);
    end
end

% If generating more than ix is passed into matlab, make a dtseries.  
% Otherwise run this code in a loop.
if size(ix,2) > 1 
    outsuffix = 'dtseries.nii';
    make_dscalars=0;
else
    outsuffix = 'dscalar.nii';
    make_dscalars=1;
end

make_dscalars

% Change smoothing kernel to a number if it is in use and is not a number 
% (i.e., if using the shell command)
if strcmp(smoothing_kernel,'none')==1 || strcmp(smoothing_kernel,'None')==1 || strcmp(smoothing_kernel,'NONE')==1
    smoothing_kernel= 'none';
elseif isnumeric(smoothing_kernel)==1
else
    smoothing_kernel = str2double(smoothing_kernel);
end


if (strcmp(smoothing_kernel,'none')~=1) && (strcmp(series,'ptseries')==1)
    disp('Check your settings. Smoothing not allowed on ptseries.');
    return
end

%wb_command = 'LD_PRELOAD=/usr/lib/x86_64-linux-gnu/libstdc++.so.6 /usr/local/bin/wb_command'; % workbench command path %rushmore
%wb_command = '/home/exacloud/lustre1/fnl_lab/code/external/utilities/workbench-9253ac2/bin_rh_linux64/wb_command';%exacloud path
%wb_command = '/Applications/workbench/bin_macosx64/wb_command'; % workbench command path

%% Load concatenated paths (i.e. paths to ciftis)

conc = strsplit(dtseries_conc_file, '.');
conc = char(conc(end));
if strcmp('conc', conc) == 1
    A = importdata(dtseries_conc_file);
    E = importdata(ptseries_or_label_conc_file);
else
    A = {dtseries_conc_file};
    E = {ptseries_or_label_conc_file};
end


%A = importdata(dtseries_conc_file);
for i = 1:length(A)
    if exist(A{i}) == 0
        NOTE = ['Subject dtseries ' num2str(i) ' does not exist']
        return
    else
    end
end
disp('All series files exist continuing ...')

%E = importdata(ptseries_or_label_conc_file);
for i = 1:length(E)
    if isnumeric (E{i}) ==1
    else
        if exist(E{i}) == 0
            NOTE = ['Subject ptseries ' num2str(i) ' does not exist']
            return
        else
        end
    end
end
disp('All series files exist continuing ...')


%% set file extensions
if series == 'ptseries'
    suffix = 'ptseries.nii';
    suffix2 = 'pconn.nii';
    % set default precision.
    % Connectivity file sizes are NOT reduced for ptseries
    output_precision = ' ';
elseif series == 'dtseries'
    suffix = 'dtseries.nii';
    suffix2 = 'dscalar.nii';
    suffix3 = 'dconn.nii';
    % set default precision.
    % Connectivity file sizes are reduced for dtseries
    output_precision = ' -cifti-output-datatype INT8 -cifti-output-range -1.0 1.0';
else
    'series needs to be "ptseries" or "dtseries"';
    return
end

%% preallocate memory if you want to do smoothing
if strcmp(smoothing_kernel,'none')==1 || strcmp(smoothing_kernel,'None')==1 || strcmp(smoothing_kernel,'NONE')==1
    %stdev_temp_filename=[dtseries_conc_file '_temp.txt'];
else
    A_smoothed = num2cell(zeros(size(A)));
    %stdev_temp_filename=[dtseries_conc_file '_temp.txt']; %rename temp file so that it can't be overwritten when the script is run in parallel.
    %C = importdata(left_surface_file); %check to make sure surface files exist
    %D = importdata(right_surface_file); %check to make sure surface files exist

    if strcmp('conc',conc) == 1
        C = importdata(left_surface_file);
        D = importdata(right_surface_file);
    else
        C = {left_surface_file};
        D = {right_surface_file};
    end


    for i = 1:length(C)
        if exist(C{i}) == 0
            NOTE = ['Subject left surface ' num2str(i) ' does not exist']
            return
        else
        end
    end
    disp('All left surface files for smoothing exist continuing ...')
    for i = 1:length(D)
        if exist(D{i}) == 0
            NOTE = ['Subject right surface ' num2str(i) ' does not exist']
            return
        else
        end
    end
    disp('All right surface files for smoothing exist continuing ...')
end



%% Generate Motion Vectors and correlation matrix

% run this if no motion censoring
if strcmp(motion_file,'none')==1 || strcmp(motion_file,'None')==1 %
    'No motion files, will use all frames to generate matrices'
    for i = 1:length(A)
        [~,output_root_name_ext,~] = fileparts(A{i});
        [~,output_root_name,~] = fileparts(output_root_name_ext);
        if strcmp(smoothing_kernel,'none')==1 || strcmp(smoothing_kernel,'None')==1 || strcmp(smoothing_kernel,'NONE')==1
            output_name = [output_dir filesep output_root_name '_all_frames_at_FD_' motion_file '_ROI' num2str(ix) '.' outsuffix];
        else
            A_smoothed{i} = [A{i}(1:length(A{i})-13) '_SMOOTHED_' num2str(smoothing_kernel) '.' suffix];
            [~,output_root_name_ext,~] = fileparts(A_smoothed{i});
            [~,output_root_name,~] = fileparts(output_root_name_ext);
            output_name = [output_dir filesep output_root_name '_all_frames_at_FD_' motion_file '_ROI' num2str(ix) '.' outsuffix];
            if exist(A_smoothed{i}) == 0 %check to see if smoothing file does not exist
                clear smoothedfile_name
                Column = 'COLUMN';
                cmd = [wb_command ' -cifti-smoothing ' A{i} ' ' num2str(smoothing_kernel) ' ' num2str(smoothing_kernel) ' ' Column ' ' A{i}(1:length(A{i})-13) '_SMOOTHED_' num2str(smoothing_kernel) '.' suffix  ' -left-surface ' C{i} '  -right-surface ' D{i}];

                system(cmd);
                clear cmd
            else %smoothed series already exists
                disp('Smoothed series already created for this subject')
            end
        end
        if exist(output_name) == 0 % make sure the dscalar doesn't already exist
            %%%%
            if strcmp(smoothing_kernel,'none')==1 || strcmp(smoothing_kernel,'None')==1 || strcmp(smoothing_kernel,'NONE')==1
                corr_pt_dt(E{i},A{i},ix,output_name,1,make_dscalars, wb_command,extraction_type,num_components,fisherz);  % Greg added the 1 on 2019-11-22 because otherwise corr_pt_dt took arguments in the wrong order
                clear mask
            else
                corr_pt_dt(E{i},A_smoothed{i},ix,output_name,1,make_dscalars, wb_command,extraction_type,num_components,fisherz);
                clear mask
            end
        else
            if strcmp(smoothing_kernel,'none')==1 || strcmp(smoothing_kernel,'None')==1 || strcmp(smoothing_kernel,'NONE')==1
                subject_conn_exists = [A{i} '_all_frames_at_FD_' motion_file '_ROI' num2str(ix) '.' outsuffix ' already exists']
            else
                subject_conn_exists = [A_smoothed{i} '_all_frames_at_FD_' motion_file '_ROI' num2str(ix) '.' outsuffix ' already exists']
            end
        end
    end
else %use motion cenosoring
    v=num2str(randi([1 10000000]));
    stdev_temp_filename=[char(dtseries_conc_file) v '_temp.txt']; %reanme tempfile so that is can't be overwritten when the script is run in parallel in the same directory.
    disp(['stdev_temp_filename file name is ' stdev_temp_filename])
    if strcmp('conc',conc) == 1
        B = importdata(motion_file);
    else
        if isnumeric(motion_file) ==1
            disp('Motion censor passed in as vector.  Assuming 1s denote "keep" and 0s denote "remove".')
            B = motion_file;
        else
            B = {motion_file};
        end
    end


    % Check that all motion files in conc file exist
    for i = 1:length(B)
        if exist(B{i}) == 0
            NOTE = ['motion file ' num2str(i) ' does not exist']
            return
        else
        end
    end
    disp('All motion files exist continuing ...')

    %Make sure length of motion files matches length of timeseries
    if length(A)==length(B)
    else
        disp('length of motion conc file does not match legnth of series conc file')
        return
    end

    %Make sure length of motion files matches length of dense timeseries
    %and parcellated
    if length(A)==length(E) && length(B)==length(E)
    else
        disp('length of motion conc file does not match legnth of either/both series conc file')
        return
    end


    %% Generate motion vector for subject and generate correlation matrix
    for i = 1:length(B)

        [~,~,motion_suffix] = fileparts(B{i}); % get file extension for motion file.

        if strcmp(motion_suffix,'.txt') %if a txt file of 0s or 1s is provided, load that. 0 means discard. 1 means keep.
            FDvec = importdata(B{i});
        else % load power motion file.
            load(B{i})

            allFD = zeros(1,length(motion_data));
            for j = 1:length(motion_data)
                allFD(j) = motion_data{j}.FD_threshold;
            end

            FDidx = find(allFD == FD_threshold);
            FDvec = motion_data{FDidx}.frame_removal;
            FDvec = abs(FDvec-1);
        end

        if exist('remove_outliers', 'var') == 0 || remove_outliers == 1
            
            disp('Removal outliers is set to true or not specified.  It will be performed by default.')
            
            % Additional frame removal based on outliers command: 
            %   isoutlier with "median" method.
            disp(A{i})
            cmd = [wb_command ' -cifti-stats ' A{i} ' -reduce STDEV > ' stdev_temp_filename];
            
            %disp(cmd)
            system(cmd);
            clear cmd
            
            % Give time to write temp file, failed to load with pause(1)
            pause(3);  
            
            disp(stdev_temp_filename)
            
            try
                STDEV_file=load(stdev_temp_filename); % load stdev of .nii file.
            catch
                disp('Attempt to load stdev file failed stdev_temp_filename.')
                disp('System may be slow- retrying in 10 seconds (1/3)...')
                pause(10);
                try
                    STDEV_file=load(stdev_temp_filename); % load stdev of .nii file.
                catch
                    disp('Attempt to load stdev file failed.')
                    disp('System may be slow- retrying in 10 seconds (2/3)...')
                    pause(10);
                    try
                        STDEV_file=load(stdev_temp_filename); % load stdev of .nii file.
                    catch
                        disp('Attempt to load stdev file failed.')
                        disp('System may be slow- retrying in 10 seconds (3/3)...')
                        pause(10);
                        try
                            STDEV_file=load(stdev_temp_filename); % load stdev of .nii file.
                        catch
                            disp(['Unable to load stdev file: ' stdev_temp_filename ])
                            disp('Exiting.')
                            return
                        end
                    end
                end
            end
            
            disp('STDEV file loaded sucessfully.')

            % clean up
            cmd = [' rm -f ' stdev_temp_filename];
            system(cmd);
            
            % disp(FDvec)

            %find the kept frames from the FD mask
            FDvec_keep_idx = find(FDvec == 1); 
            
            Outlier_file=isthisanoutlier(STDEV_file(FDvec_keep_idx), ...
                                                    'median'); 
            %find outlier indices
            Outlier_idx=find(Outlier_file==1); 
            
            %set outliers to zero within FDvec
            FDvec(FDvec_keep_idx(Outlier_idx))=0; 
            %clear STDEV_file FDvec_keep_idx Outlier_file Outlier_idx
           
        else exist('remove_outliers', 'var') == 1 && remove_outliers == 0;
            disp('Motion censoring performed on FD alone. Frames with outliers in BOLD std dev not removed');
        end

        mask=logical(FDvec); %used in corr_pt_dt_masking.
        good_frames_idx = find(FDvec == 1);
        good_minutes = (length(good_frames_idx)*TR)/60; % number of good minutes in your data
        if strcmp(minutes_limit,'none')==1 || strcmp(minutes_limit,'None')==1 || strcmp(minutes_limit,'NONE')==1
            fileID = fopen([B{i} '_' num2str(FD_threshold) '_cifti_censor_FD_vector_All_Good_Frames.txt'],'w');
            fprintf(fileID,'%1.0f\n',FDvec);
            fclose(fileID);

            if strcmp(smoothing_kernel,'none')==1 || strcmp(smoothing_kernel,'None')==1 || strcmp(smoothing_kernel,'NONE')==1
                [~,output_root_name_ext,~] = fileparts(A{i});
                [~,output_root_name,~] = fileparts(output_root_name_ext);
                output_name = [output_dir filesep output_root_name '_all_frames_at_FD_' num2str(FD_threshold) '_ROI' num2str(ix) '.' outsuffix];
                %temp_name = [A{i} '_all_frames_at_FD_' num2str(FD_threshold) '_ROI' num2str(ix) '.' suffix2];
            else

                A_smoothed{i} = [A{i}(1:length(A{i})-13) '_SMOOTHED_' num2str(smoothing_kernel) '.' suffix];
                [~,output_root_name_ext,~] = fileparts(A_smoothed{i});
                [~,output_root_name,~] = fileparts(output_root_name_ext);
                output_name = [output_dir filesep output_root_name '_all_frames_at_FD_' num2str(FD_threshold) '_ROI' num2str(ix) '.' outsuffix];
                %temp_name = [A_smoothed{i} '_all_frames_at_FD_' num2str(FD_threshold) '_ROI' num2str(ix) '.' suffix2];
                if exist(A_smoothed{i}) ==0 %check to see if smoothed file exists yet.
                    Column = 'COLUMN';
                    %cmd = [wb_command ' -cifti-smoothing ' A{i} ' ' smoothing_kernel ' ' smoothing_kernel ' ' Column ' ' A{i}(1:length(A{i})-13) '_SMOOTHED_' num2str(smoothing_kernel) '.' suffix  ' -left-surface ' C{i} '  -right-surface ' D{i}];
                    cmd = [wb_command ' -cifti-smoothing ' A{i} ' ' num2str(smoothing_kernel) ' ' num2str(smoothing_kernel) ' ' Column ' ' A{i}(1:length(A{i})-13) '_SMOOTHED_' num2str(smoothing_kernel) '.' suffix  ' -left-surface ' C{i} '  -right-surface ' D{i}];
                    system(cmd);
                    clear cmd
                else %smoothed series already exists
                    disp('Smoothed dtseries already created for this subject')
                end
            end

            if exist(output_name) == 0 % if the matrix already exists skip
                if strcmp(smoothing_kernel,'none')==1 || strcmp(smoothing_kernel,'None')==1 || strcmp(smoothing_kernel,'NONE')==1
                    %corr_pt_dt_exaversion(E{i},A{i},ix,temp_name,mask,make_dscalars,wb_command);
                    corr_pt_dt(E{i},A{i},ix,output_name,mask,make_dscalars, wb_command,extraction_type,num_components,fisherz);
                    %disp([temp_name 'created'])
                    clear mask
                    clear cmd
                else
                    %corr_pt_dt(E{i},A_smoothed{i},ix,temp_name,mask,make_dscalars,wb_command);
                    corr_pt_dt(E{i},A_smoothed{i},ix,output_name,mask,make_dscalars, wb_command,extraction_type,num_components,fisherz);
                    %disp([temp_name 'created'])
                    clear mask

                    clear cmd
                end

            else
                if strcmp(smoothing_kernel,'none')==1 || strcmp(smoothing_kernel,'None')==1 || strcmp(smoothing_kernel,'NONE')==1
                    subject_conn_exists = [A{i} '_all_frames_at_FD_' num2str(FD_threshold) '_ROI' num2str(ix) '.' outsuffix ' already exists'];
                else
                    subject_conn_exists = [A_smoothed{i} '_all_frames_at_FD_' num2str(FD_threshold) '_ROI' num2str(ix) '.' outsuffix ' already exists'];
                end
            end

        else %Start minutes limit calculation

            %% <30 seconds
            if good_minutes < 0.5 % if there is less than 30 seconds, don't generate the correlation matrix
                subject_has_too_few_frames = ['Subject ' num2str(i) ' has less than 30 seconds of good data']


            %% All_frames
            elseif minutes_limit > good_minutes % if there is not enough data for your subject, just generate the matrix with all available frames
                fileID = fopen([B{i} '_' num2str(FD_threshold) '_cifti_censor_FD_vector_All_Good_Frames.txt'],'w');
                fprintf(fileID,'%1.0f\n',FDvec);
                fclose(fileID);

                if strcmp(smoothing_kernel,'none')==1 || strcmp(smoothing_kernel,'None')==1 || strcmp(smoothing_kernel,'NONE')==1
                    [~,output_root_name_ext,~] = fileparts(A{i});
                    [~,output_root_name,~] = fileparts(output_root_name_ext);
                    output_name = [output_dir filesep output_root_name '_all_frames_at_FD_' num2str(FD_threshold) '_ROI' num2str(ix) '.' outsuffix];
                else
                    A_smoothed{i} = [A{i}(1:length(A{i})-13) '_SMOOTHED_' num2str(smoothing_kernel) '.' suffix];
                    [~,output_root_name_ext,~] = fileparts(A_smoothed{i});
                    [~,output_root_name,~] = fileparts(output_root_name_ext);
                    output_name = [output_dir filesep output_root_name '_all_frames_at_FD_' num2str(FD_threshold) '_ROI' num2str(ix) '.' outsuffix];
                    if exist(A_smoothed{i}) ==0 %check to see if smoothed file exists yet.
                        Column = 'COLUMN';
                        %cmd = [wb_command ' -cifti-smoothing ' A{i} ' ' smoothing_kernel ' ' smoothing_kernel ' ' Column ' ' A{i}(1:length(A{i})-13) '_SMOOTHED_' num2str(smoothing_kernel) '.' suffix  ' -left-surface ' C{i} '  -right-surface ' D{i}];
                        cmd = [wb_command ' -cifti-smoothing ' A{i} ' ' num2str(smoothing_kernel) ' ' num2str(smoothing_kernel) ' ' Column ' ' A{i}(1:length(A{i})-13) '_SMOOTHED_' num2str(smoothing_kernel) '.' suffix  ' -left-surface ' C{i} '  -right-surface ' D{i}];
                        system(cmd);
                        clear cmd
                    else %smoothed series already exists
                        disp('Smoothed dtseries already created for this subject')
                    end
                end

                if exist(output_name) == 0 % if the matrix already exists skip
                    if strcmp(smoothing_kernel,'none')==1 || strcmp(smoothing_kernel,'None')==1 || strcmp(smoothing_kernel,'NONE')==1
                        corr_pt_dt(E{i},A{i},ix,output_name,mask,make_dscalars, wb_command,extraction_type,num_components,fisherz);
                        clear mask cmd
                    else
                        corr_pt_dt(E{i},A_smoothed{i},ix,output_name,mask,make_dscalars, wb_command,extraction_type,num_components,fisherz);
                        clear mask cmd
                    end

                else
                    if strcmp(smoothing_kernel,'none')==1 || strcmp(smoothing_kernel,'None')==1 || strcmp(smoothing_kernel,'NONE')==1
                        subject_conn_exists = [A{i} '_all_frames_at_FD_' num2str(FD_threshold) '_ROI' num2str(ix) '.' outsuffix ' already exists'];
                    else
                        subject_conn_exists = [A_smoothed{i} '_all_frames_at_FD_' num2str(FD_threshold) '_ROI' num2str(ix) '.' outsuffix ' already exists'];
                    end
                end

            %% X minutes
            elseif minutes_limit <= good_minutes % if there is enough data, match the amount of data used for your subject matrix to the minutes_limit
                good_frames_needed = round(minutes_limit*60/TR); %number of good frames to randomly pull
                rand_good_frames = sort(randperm(length(good_frames_idx),good_frames_needed));
                FDvec_cut = zeros(length(FDvec),1);
                ones_idx = good_frames_idx(rand_good_frames);
                FDvec_cut(ones_idx) = 1; % the new vector that should match good frames with the minutes limit
                mask=logical(FDvec_cut); %used in corr_pt_dt_masking.

                fileID = fopen([B{i} '_' num2str(FD_threshold) '_cifti_censor_FD_vector_' num2str(minutes_limit) '_minutes_of_data_at_' num2str(FD_threshold) '_threshold.txt'],'w');
                fprintf(fileID,'%1.0f\n',FDvec_cut);
                fclose(fileID);
                if strcmp(smoothing_kernel,'none')==1 || strcmp(smoothing_kernel,'None')==1 || strcmp(smoothing_kernel,'NONE')==1
                    [~,output_root_name_ext,~] = fileparts(A{i});
                    [~,output_root_name,~] = fileparts(output_root_name_ext);
                    output_name = [output_dir filesep output_root_name '_' num2str(minutes_limit) '_minutes_of_data_at_FD_' num2str(FD_threshold) '_ROI' num2str(ix) '.' outsuffix];
                else % Smooth
                    A_smoothed{i} = [A{i}(1:length(A{i})-13) '_SMOOTHED_' num2str(smoothing_kernel) '.' suffix];
                    [~,output_root_name_ext,~] = fileparts(A_smoothed{i});
                    [~,output_root_name,~] = fileparts(output_root_name_ext);
                    output_name = [output_dir filesep output_root_name '_' num2str(minutes_limit) '_minutes_of_data_at_FD_' num2str(FD_threshold) '_ROI' num2str(ix) '.' outsuffix];
                    if exist(A_smoothed{i}) == 0
                        Column = 'COLUMN';
                        cmd = [wb_command ' -cifti-smoothing ' A{i} ' ' num2str(smoothing_kernel) ' ' num2str(smoothing_kernel) ' ' Column ' ' A{i}(1:length(A{i})-13) '_SMOOTHED_' num2str(smoothing_kernel) '.' suffix  ' -left-surface ' C{i} '  -right-surface ' D{i}];
                        system(cmd);
                        clear cmd
                    else %smoothed series already exists
                        disp('Smoothed dtseries already created for this subject')
                    end
                end
                
                if exist(output_name) == 0 % check to see if the file already exists
                    if strcmp(smoothing_kernel,'none') == 1 || strcmp(smoothing_kernel,'None')==1 || strcmp(smoothing_kernel,'NONE')==1
                        corr_pt_dt(E{i},A{i},ix,output_name,mask,make_dscalars, wb_command,extraction_type,num_components,fisherz);
                        clear mask
                    else
                        corr_pt_dt(E{i},A_smoothed{i},ix,output_name,mask,make_dscalars, wb_command,extraction_type,num_components,fisherz);
                        clear mask
                    end
                else
                    if strcmp(smoothing_kernel,'none')==1 || strcmp(smoothing_kernel,'None')==1 || strcmp(smoothing_kernel,'NONE')==1
                        disp([A{i} '_' num2str(minutes_limit) '_minutes_of_data_at_FD_' num2str(FD_threshold) '_ROI' num2str(ix) '.' outsuffix ' already exists'])
                    else
                        disp([A_smoothed{i} '_' num2str(minutes_limit) '_minutes_of_data_at_FD_' num2str(FD_threshold) '_ROI' num2str(ix) '.' outsuffix ' already exists'])
                    end
                end
            else
                disp('Something is wrong with regard to number of good minutes calculation.')
            end
        end
    end
end

disp('Done running cifti_conn_matrix_to_corr_pt_dt.m')
