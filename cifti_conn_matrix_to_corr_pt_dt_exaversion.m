function  cifti_conn_matrix_to_corr_pt_dt_exaversion(dtseries_conc_file,ptseries_conc_file,motion_file, FD_threshold, TR, ix, minutes_limit, smoothing_kernal,left_surface_file, right_surface_file,other_motion_mask,debugonrush)

%R. Hermosillo 6/15/2018
%Added an option to supply the code with an external mask that contains a vector of 1s and 0s for all participants.(in .mat format) (rather than calculating the mask from power2014_FDonly.mat).

%R. Hermosillo 2/19/2018
%Modified to accept index as an input arguement.  Check for arguments as strings and converts them to numbers (in case a compiled version is used).

%dtseries_conc_file = dense timeseries parcellated timeseries conc file %(i.e. text file with paths to each dtseries)
%ptseries_conc_file = parcellated timeseries parcellated timeseries conc file %(i.e. text file with paths to each ptseries)
%motion_file = .conc file that points to the FNL's power2014_FDonly.mat
%files for each dt or ptseries (note: if no motion file to be used, type 'none'). You can also supply a .mat file that contains masks for all subjects.
%FD threshold = specify motion threshold (e.g. 0.2)
%TR = TR of your data
%ix = index of your ROI in the parcelation (i.e.here ROIS 342= ACCUMBENS_LEFT and 162=162_R_Default 341=Amygdala-L, 350=Amygdala-R
%minutes_limit = specify the number of minutes to be used to generate the correlation matrix
%smoothing_kernal = specify smoothing kernal (note: if no smoothing file to be used, type 'none')
%L_surfacefile = path to a conc of L midthickness file
%R_surface_file = path to a conc of R midthickness file
%other_motion_mask = 1 if external mask is supplied. %%NOTE: this will override FD settings. 0 if you want to specify which frames to use based on FD provided.
%debugonrush = 1 is you want to debug on rushmore, 0 or empty for compiled code.

% example:
% cifti_conn_matrix('/mnt/max/shared/code/internal/utilities/hcp_comm_det_damien/ADHD_DVARS_group_dtseries.conc','dtseries','/mnt/max/shared/code/internal/utilities/hcp_comm_det_damien/ADHD_DVARS_group_motion.conc',0.2,2.5,5,2.55,'/mnt/max/shared/code/internal/utilities/hcp_comm_det_damien/ADHD_DVARS_group_right_midthickness_surfaces.conc','/mnt/max/shared/code/internal/utilities/hcp_comm_det_damien/ADHD_DVARS_group_right_midthickness_surfaces.conc',1,0);
% example without smoothing:
% cifti_conn_matrix('/mnt/max/shared/code/internal/utilities/hcp_comm_det_damien/ADHD_DVARS_group_dtseries.conc','dtseries','/mnt/max/shared/code/internal/utilities/hcp_comm_det_damien/ADHD_DVARS_group_motion.conc',0.2,2.5,5,'none','none','none',1,0);


%Uncomment for debugging
% dtseries_conc_file='/mnt/max/shared/projects/ADHD_comm_det/ADHD_QC_7-21-17/ADHD_good_resting_full_list_from_QC_7-21-17_dtseries.conc';
% ptseries_conc_file='/mnt/max/shared/projects/ADHD_comm_det/ADHD_QC_7-21-17/ADHD_good_resting_full_list_from_QC_7-21-17_ptseries.conc';
% motion_file= '/mnt/max/shared/projects/ADHD_comm_det/ADHD_QC_7-21-17/ADHD_good_resting_full_list_from_QC_7-21-17_motion.conc';
% %motion_file='none';
% FD_threshold=0.2;
% TR=2.5;
% ix = 343;
% minutes_limit=4;
% smoothing_kernal= '2.55'; %'none'
% %smoothing_kernal='none';
% left_surface_file='/mnt/max/shared/projects/ADHD_comm_det/ADHD_QC_7-21-17/ADHD_good_resting_full_list_from_QC_7-21-17_leftmid_surf.conc';
% right_surface_file='/mnt/max/shared/projects/ADHD_comm_det/ADHD_QC_7-21-17/ADHD_good_resting_full_list_from_QC_7-21-17_rightmid_surf.conc';
% other_motion_mask = 1;
% debugonrush = 1;
%% Define the seed ROIs
%ix=[350]; % selecting here ROIS 342) ACCUMBENS_LEFT and 162)162_R_Default 341=Amygdala-L, 350=Amygdala-R

%convert number variables to cnumbers (only within compiled code);
if isnumeric(ix)==1
else
    ix = str2num(ix);
end

if isnumeric(FD_threshold)==1
else
    FD_threshold=str2num(FD_threshold);
end

if isnumeric(TR)==1
else
    TR=str2num(TR);
end

%Check to make sure that  minutes limit is a number (unless you've set it
%to 'none')
if strcmp(minutes_limit,'none')==1 || strcmp(minutes_limit,'None')==1 || strcmp(minutes_limit,'NONE')==1
    minutes_limit= 'none';
elseif isnumeric(minutes_limit)==1
    disp('minutes limit is passed in as numeric.')
else
    disp('minutes limit is passed in as string. converting to numeric')
    minutes_limit = str2num(minutes_limit);
end


make_dscalars=1; % if make scalars =1, then the output file will be row of correlation values as a dscalar.  If 0, then correlations will be saved as a dtseries.
if make_dscalars==1
else
    make_dscalars=[];
end

if isnumeric(other_motion_mask)==1
else
    other_motion_mask=str2num(other_motion_mask);
end

if isnumeric(debugonrush)==1
else
    debugonrush=str2num(debugonrush);
end

series='dtseries'; %DO NOT change file extention here.

%% Change smoothing kernal to a number if it  is in use and is not a number (i.e.if using the shell command)
if strcmp(smoothing_kernal,'none')==1 || strcmp(smoothing_kernal,'None')==1 || strcmp(smoothing_kernal,'NONE')==1
    smoothing_kernal= 'none';
elseif isnumeric(smoothing_kernal)==1
else
    smoothing_kernal = str2num(smoothing_kernal);
end


if (strcmp(smoothing_kernal,'none')~=1) && (strcmp(series,'ptseries')==1)
    disp('Check your settings. Smoothing not allowed on ptseries.');
    return
else
end

%%
if debugonrush ==1
    %wb_command = '/Applications/workbench/bin_macosx64/wb_command'; % workbench command path
    wb_command = 'LD_PRELOAD=/usr/lib/x86_64-linux-gnu/libstdc++.so.6 /usr/local/bin/wb_command'; % workbench command path %rushmore
else
    wb_command = '/home/exacloud/lustre1/fnl_lab/code/external/utilities/workbench-9253ac2/bin_rh_linux64/wb_command';%exacloud path
end

%% Load concatenated paths (i.e. paths to ciftis)

A = importdata(dtseries_conc_file);
for i = 1:length(A)
    if exist(A{i}) == 0
        NOTE = ['Subject dtseries ' num2str(i) ' does not exist']
        return
    else
    end
end
disp('All dt series files exist continuing ...')

E = importdata(ptseries_conc_file);
for i = 1:length(E)
    if exist(E{i}) == 0
        NOTE = ['Subject ptseries ' num2str(i) ' does not exist']
        return
    else
    end
end
disp('All pt series files exist continuing ...')


%% set file extensions
if series == 'ptseries'
    suffix = 'ptseries.nii';
    suffix2 = 'pconn.nii';
    %suffix2 = 'pconn.nii';
    %suffix3 = 'pconn';
    output_precision = ' '; %set default precision. Connectivity file sizes are NOT reduced for ptseries
elseif series == 'dtseries'
    suffix = 'dtseries.nii';
    suffix2 = 'dscalar.nii';
    %suffix2 = 'dconn.nii';
    %suffix3 = 'dconn';
    output_precision = ' -cifti-output-datatype INT8 -cifti-output-range -1.0 1.0'; %set default precision. Connectivity file sizes are reduced for dtseries
else
    'series needs to be "ptseries" or "dtseries"';
    return
end

%% prealocate memory if you want to do smoothing
if strcmp(smoothing_kernal,'none')==1 || strcmp(smoothing_kernal,'None')==1 || strcmp(smoothing_kernal,'NONE')==1
    stdev_temp_filename=[dtseries_conc_file '_temp.txt'];
else
    A_smoothed = num2cell(zeros(size(A)));
    stdev_temp_filename=[dtseries_conc_file '_temp.txt']; %rename temp file so that it can't be overwritten when the script is run in parallel.
    C = importdata(left_surface_file); %check to make sure surface files exist
    D = importdata(right_surface_file); %check to make sure surface files exist
    
    for i = 1:length(C)
        if exist(C{i}) == 0
            NOTE = ['Subject left surface ' num2str(i) ' does not exist']
            return
        else
        end
    end
    disp('All left surface files for smoothing exist continuing ...')
    for i = 1:length(D)
        if exist(D{i}) == 0
            NOTE = ['Subject right surface ' num2str(i) ' does not exist']
            return
        else
        end
    end
    disp('All right surface files for smoothing exist continuing ...')
end

%% Generate Motion Vectors and correlation matrix

if strcmp(motion_file,'none')==1 || strcmp(motion_file,'None')==1 % run this if no motion censoring
    'No motion files, will use all frames to generate matrices'
    for i = 1:length(A)
        if strcmp(smoothing_kernal,'none')==1 || strcmp(smoothing_kernal,'None')==1 || strcmp(smoothing_kernal,'NONE')==1
            temp_name = [A{i} '_all_frames_at_FD_' motion_file '_ROI' num2str(ix) '.' suffix2];
        else
            A_smoothed{i} = [A{i}(1:length(A{i})-13) '_SMOOTHED_' num2str(smoothing_kernal) '.' suffix];
            temp_name = [A_smoothed{i} '_all_frames_at_FD_' motion_file '_ROI' num2str(ix) '.' suffix2];
            if exist(A_smoothed{i}) == 0 %check to see if smoothing file does not exist
                clear smoothedfile_name
                Column = 'COLUMN';
                %cmd = [wb_command ' -cifti-smoothing ' A{i} ' ' smoothing_kernal ' ' smoothing_kernal ' ' Column ' ' A{i}(1:length(A{i})-13) '_SMOOTHED_' num2str(smoothing_kernal) '.' suffix  ' -left-surface ' C{i} '  -right-surface ' D{i}];
                cmd = [wb_command ' -cifti-smoothing ' A{i} ' ' num2str(smoothing_kernal) ' ' num2str(smoothing_kernal) ' ' Column ' ' A{i}(1:length(A{i})-13) '_SMOOTHED_' num2str(smoothing_kernal) '.' suffix  ' -left-surface ' C{i} '  -right-surface ' D{i}];
                
                system(cmd);
                clear cmd
            else %smoothed series already exists
                disp('Smoothed series already created for this subject')
            end
        end
        if exist(temp_name) == 0 % make sure the matrix doesn't already exist
            %%%%
            if strcmp(smoothing_kernal,'none')==1 || strcmp(smoothing_kernal,'None')==1 || strcmp(smoothing_kernal,'NONE')==1
                %cmd = [wb_command output_precision ' -cifti-correlation ' A{i} ' ' A{i} '_all_frames_at_FD_' motion_file '.' suffix2  ' -fisher-z'];
                %cmd = [wb_command ' -cifti-correlation ' A{i} ' ' A{i} '_all_frames_at_FD_' motion_file '.' suffix2  ' -fisher-z'];
                %mask=ones(size(A(:,i))); %set columns to 1 (i.e. all are  useble)
                corr_pt_dt_exaversion(E{i},A{i},ix,temp_name,make_dscalars,wb_command);
                %disp([temp_name 'created'])
                clear mask
                %tic;
                %system(cmd);
                %toc;
                %clear cmd
            else
                corr_pt_dt_exaversion(E{i},A{i},ix,temp_name,make_dscalars,wb_command);
                %disp([temp_name 'created'])
                clear mask
                %cmd = [wb_command output_precision ' -cifti-correlation ' A_smoothed{i} ' ' A_smoothed{i} '_all_frames_at_FD_' motion_file '.' suffix2  ' -fisher-z'];
                %cmd = [wb_command ' -cifti-correlation ' A{i} ' ' A{i} '_all_frames_at_FD_' motion_file '.' suffix2  ' -fisher-z'];
                %                 tic;
                %                 system(cmd);
                %                 toc;
                %                 clear cmd
            end
        else
            if strcmp(smoothing_kernal,'none')==1 || strcmp(smoothing_kernal,'None')==1 || strcmp(smoothing_kernal,'NONE')==1
                subject_conn_exists = [A{i} '_all_frames_at_FD_' motion_file '_ROI' num2str(ix) '.' suffix2 ' already exists']
            else
                subject_conn_exists = [A_smoothed{i} '_all_frames_at_FD_' motion_file '_ROI' num2str(ix) '.' suffix2 ' already exists']
            end
        end
    end
else %use motion cenosoring
    B = importdata(motion_file);
    % Check that all motion files in conc file exist
    for i = 1:length(B)
        if exist(B{i}) == 0
            NOTE = ['motion file ' num2str(i) ' does not exist']
            return
        else
        end
    end
    disp('All motion files exist continuing ...')
    
    %Make sure length of motion files matches length of timeseries
    if length(A)==length(B)
    else
        disp('length of motion conc file does not match legnth of series conc file')
        return
    end
    
    %Make sure length of motion files matches length of dense timeseries
    %and parcellated
    if length(A)==length(E) && length(B)==length(E)
    else
        disp('length of motion file (or size of values in that .mat file) does not match length of either/both series conc file')
        return
    end
    
    %% Generate motion vector for subject and generate correlation matrix
    for i = 1:length(B)
        
        if other_motion_mask ==1 %use an external mask rather than calculating the mask here.
            FDvec=B{i};
        else
            load(B{i})
            allFD = zeros(1,length(motion_data));
            for j = 1:length(motion_data)
                allFD(j) = motion_data{j}.FD_threshold;
            end
            FDidx = find(allFD == FD_threshold);
            FDvec = motion_data{FDidx}.frame_removal;
            FDvec = abs(FDvec-1);
            
            %% additional frame removal based on Outliers command: isoutlier with "median" method.
            cmd = [wb_command ' -cifti-stats ' A{i} ' -reduce STDEV > ' stdev_temp_filename];
            system(cmd);
            clear cmd
            pause(3);  % to give time to write temp file, failed to load with pause(1)
            STDEV_file=load(stdev_temp_filename); % load stdev of .nii file.
            FDvec_keep_idx = find(FDvec==1); %find the kept frames from the FD mask
            Outlier_file=isthisanoutlier(STDEV_file(FDvec_keep_idx),'median'); %find outlier %use "isoutlier" on rushmore or "isthisanoutlier" on exacloud (they should have the same results though).
            Outlier_idx=find(Outlier_file==1); %find outlier indices
            FDvec(FDvec_keep_idx(Outlier_idx))=0; %set outliers to zero within FDvec
            %clear STDEV_file FDvec_keep_idx Outlier_file Outlier_idx
            %%
        end
        
        mask=logical(FDvec); %used in corr_pt_dt_exaversion_masking.
        good_frames_idx = find(FDvec == 1);
        good_minutes = (length(good_frames_idx)*TR)/60; % number of good minutes in your data
        
        if strcmp(minutes_limit,'none')==1 || strcmp(minutes_limit,'None')==1 || strcmp(minutes_limit,'NONE')==1
            fileID = fopen([B{i} '_' num2str(FD_threshold) '_cifti_censor_FD_vector_All_Good_Frames.txt'],'w');
            fprintf(fileID,'%1.0f\n',FDvec);
            fclose(fileID);
            
            if strcmp(smoothing_kernal,'none')==1 || strcmp(smoothing_kernal,'None')==1 || strcmp(smoothing_kernal,'NONE')==1
                temp_name = [A{i} '_all_frames_at_FD_' num2str(FD_threshold) '_ROI' num2str(ix) '.' suffix2];
            else
                A_smoothed{i} = [A{i}(1:length(A{i})-13) '_SMOOTHED_' num2str(smoothing_kernal) '.' suffix];
                temp_name = [A_smoothed{i} '_all_frames_at_FD_' num2str(FD_threshold) '_ROI' num2str(ix) '.' suffix2];
                if exist(A_smoothed{i}) ==0 %check to see if smoothed file exists yet.
                    Column = 'COLUMN';
                    %cmd = [wb_command ' -cifti-smoothing ' A{i} ' ' smoothing_kernal ' ' smoothing_kernal ' ' Column ' ' A{i}(1:length(A{i})-13) '_SMOOTHED_' num2str(smoothing_kernal) '.' suffix  ' -left-surface ' C{i} '  -right-surface ' D{i}];
                    cmd = [wb_command ' -cifti-smoothing ' A{i} ' ' num2str(smoothing_kernal) ' ' num2str(smoothing_kernal) ' ' Column ' ' A{i}(1:length(A{i})-13) '_SMOOTHED_' num2str(smoothing_kernal) '.' suffix  ' -left-surface ' C{i} '  -right-surface ' D{i}];
                    system(cmd);
                    clear cmd
                else %smoothed series already exists
                    disp('Smoothed dtseries already created for this subject')
                end
            end
            
            if exist(temp_name) == 0 % if the matrix already exists skip
                if strcmp(smoothing_kernal,'none')==1 || strcmp(smoothing_kernal,'None')==1 || strcmp(smoothing_kernal,'NONE')==1
                    corr_pt_dt_exaversion(E{i},A{i},ix,temp_name,mask,make_dscalars,wb_command);
                    %disp([temp_name 'created'])
                    clear mask
                    %cmd = [wb_command output_precision ' -cifti-correlation ' A{i} ' ' A{i} '_all_frames_at_FD_' num2str(FD_threshold) '.' suffix2 ' -weights ' B{i} '_' num2str(FD_threshold) '_cifti_censor_FD_vector_All_Good_Frames.txt -fisher-z'];
                    %cmd = [wb_command ' -cifti-correlation ' A{i} ' ' A{i} '_all_frames_at_FD_' num2str(FD_threshold) '.' suffix2 ' -weights ' B{i} '_' num2str(FD_threshold) '_cifti_censor_FD_vector_All_Good_Frames.txt -fisher-z'];
                    %tic;
                    %system(cmd);
                    %toc;
                    clear cmd
                else
                    corr_pt_dt_exaversion(E{i},A_smoothed{i},ix,temp_name,mask,make_dscalars,wb_command);
                    %disp([temp_name 'created'])
                    clear mask
                    %cmd = [wb_command output_precision ' -cifti-correlation ' A_smoothed{i} ' ' A_smoothed{i} '_all_frames_at_FD_' num2str(FD_threshold) '.' suffix2 ' -weights ' B{i} '_' num2str(FD_threshold) '_cifti_censor_FD_vector_All_Good_Frames.txt -fisher-z'];
                    %tic;
                    %system(cmd);
                    %toc;
                    clear cmd
                end
                
            else
                if strcmp(smoothing_kernal,'none')==1 || strcmp(smoothing_kernal,'None')==1 || strcmp(smoothing_kernal,'NONE')==1
                    subject_conn_exists = [A{i} '_all_frames_at_FD_' num2str(FD_threshold) '_ROI' num2str(ix) '.' suffix2 ' already exists'];
                else
                    subject_conn_exists = [A_smoothed{i} '_all_frames_at_FD_' num2str(FD_threshold) '_ROI' num2str(ix) '.' suffix2 ' already exists'];
                end
            end
            
        else %Start minutes limit calculation
            
            
            %% <30 seconds
            if good_minutes < 0.5 % if there is less than 30 seconds, don't generate the correlation matrix
                subject_has_too_few_frames = ['Subject ' num2str(i) ' has less than 30 seconds of good data']
                
                
                %% All_frames
            elseif minutes_limit > good_minutes % if there is not enough data for your subject, just generate the matrix with all available frames
                fileID = fopen([B{i} '_' num2str(FD_threshold) '_cifti_censor_FD_vector_All_Good_Frames.txt'],'w');
                fprintf(fileID,'%1.0f\n',FDvec);
                fclose(fileID);
                
                if strcmp(smoothing_kernal,'none')==1 || strcmp(smoothing_kernal,'None')==1 || strcmp(smoothing_kernal,'NONE')==1
                    temp_name = [A{i} '_all_frames_at_FD_' num2str(FD_threshold) '_ROI' num2str(ix) '.' suffix2];
                else
                    A_smoothed{i} = [A{i}(1:length(A{i})-13) '_SMOOTHED_' num2str(smoothing_kernal) '.' suffix];
                    temp_name = [A_smoothed{i} '_all_frames_at_FD_' num2str(FD_threshold) '_ROI' num2str(ix) '.' suffix2];
                    if exist(A_smoothed{i}) ==0 %check to see if smoothed file exists yet.
                        Column = 'COLUMN';
                        %cmd = [wb_command ' -cifti-smoothing ' A{i} ' ' smoothing_kernal ' ' smoothing_kernal ' ' Column ' ' A{i}(1:length(A{i})-13) '_SMOOTHED_' num2str(smoothing_kernal) '.' suffix  ' -left-surface ' C{i} '  -right-surface ' D{i}];
                        cmd = [wb_command ' -cifti-smoothing ' A{i} ' ' num2str(smoothing_kernal) ' ' num2str(smoothing_kernal) ' ' Column ' ' A{i}(1:length(A{i})-13) '_SMOOTHED_' num2str(smoothing_kernal) '.' suffix  ' -left-surface ' C{i} '  -right-surface ' D{i}];
                        system(cmd);
                        clear cmd
                    else %smoothed series already exists
                        disp('Smoothed dtseries already created for this subject')
                    end
                end
                
                if exist(temp_name) == 0 % if the matrix already exists skip
                    if strcmp(smoothing_kernal,'none')==1 || strcmp(smoothing_kernal,'None')==1 || strcmp(smoothing_kernal,'NONE')==1
                        corr_pt_dt_exaversion(E{i},A{i},ix,temp_name,mask,make_dscalars,wb_command);
                        %disp([temp_name 'created'])
                        clear mask
                        %cmd = [wb_command output_precision ' -cifti-correlation ' A{i} ' ' A{i} '_all_frames_at_FD_' num2str(FD_threshold) '.' suffix2 ' -weights ' B{i} '_' num2str(FD_threshold) '_cifti_censor_FD_vector_All_Good_Frames.txt -fisher-z'];
                        %cmd = [wb_command ' -cifti-correlation ' A{i} ' ' A{i} '_all_frames_at_FD_' num2str(FD_threshold) '.' suffix2 ' -weights ' B{i} '_' num2str(FD_threshold) '_cifti_censor_FD_vector_All_Good_Frames.txt -fisher-z'];
                        %tic;
                        %system(cmd);
                        %toc;
                        clear cmd
                    else
                        corr_pt_dt_exaversion(E{i},A_smoothed{i},ix,temp_name,mask,make_dscalars,wb_command);
                        %disp([temp_name 'created'])
                        clear mask
                        %cmd = [wb_command output_precision ' -cifti-correlation ' A_smoothed{i} ' ' A_smoothed{i} '_all_frames_at_FD_' num2str(FD_threshold) '.' suffix2 ' -weights ' B{i} '_' num2str(FD_threshold) '_cifti_censor_FD_vector_All_Good_Frames.txt -fisher-z'];
                        %tic;
                        %system(cmd);
                        %toc;
                        clear cmd
                    end
                    
                else
                    if strcmp(smoothing_kernal,'none')==1 || strcmp(smoothing_kernal,'None')==1 || strcmp(smoothing_kernal,'NONE')==1
                        subject_conn_exists = [A{i} '_all_frames_at_FD_' num2str(FD_threshold) '_ROI' num2str(ix) '.' suffix2 ' already exists'];
                    else
                        subject_conn_exists = [A_smoothed{i} '_all_frames_at_FD_' num2str(FD_threshold) '_ROI' num2str(ix) '.' suffix2 ' already exists'];
                    end
                end
                
                %% X minutes
            elseif minutes_limit <= good_minutes % if there is enough data, match the amount of data used for your subject matrix to the minutes_limit
                good_frames_needed = round(minutes_limit*60/TR); %number of good frames to randomly pull
                rand_good_frames = sort(randperm(length(good_frames_idx),good_frames_needed));
                FDvec_cut = zeros(length(FDvec),1);
                ones_idx = good_frames_idx(rand_good_frames);
                FDvec_cut(ones_idx) = 1; % the new vector that should match good frames with the minutes limit
                
                fileID = fopen([B{i} '_' num2str(FD_threshold) '_cifti_censor_FD_vector_' num2str(minutes_limit) '_minutes_of_data_at_' num2str(FD_threshold) '_threshold.txt'],'w');
                fprintf(fileID,'%1.0f\n',FDvec_cut);
                fclose(fileID);
                if strcmp(smoothing_kernal,'none')==1 || strcmp(smoothing_kernal,'None')==1 || strcmp(smoothing_kernal,'NONE')==1
                    temp_name = [A{i} '_' num2str(minutes_limit) '_minutes_of_data_at_FD_' num2str(FD_threshold) '_ROI' num2str(ix) '.' suffix2];
                else % Smooth
                    A_smoothed{i} = [A{i}(1:length(A{i})-13) '_SMOOTHED_' num2str(smoothing_kernal) '.' suffix];
                    temp_name = [A_smoothed{i} '_' num2str(minutes_limit) '_minutes_of_data_at_FD_' num2str(FD_threshold) '_ROI' num2str(ix) '.' suffix2];
                    if exist(A_smoothed{i}) == 0
                        Column = 'COLUMN';
                        %cmd = [wb_command ' -cifti-smoothing ' A{i} ' ' smoothing_kernal ' ' smoothing_kernal ' ' Column ' ' A{i}(1:length(A{i})-13) '_SMOOTHED_' num2str(smoothing_kernal) '.' suffix  ' -left-surface ' C{i} '  -right-surface ' D{i}];
                        cmd = [wb_command ' -cifti-smoothing ' A{i} ' ' num2str(smoothing_kernal) ' ' num2str(smoothing_kernal) ' ' Column ' ' A{i}(1:length(A{i})-13) '_SMOOTHED_' num2str(smoothing_kernal) '.' suffix  ' -left-surface ' C{i} '  -right-surface ' D{i}];
                        
                        system(cmd);
                        clear cmd
                    else %smoothed series already exists
                        disp('Smoothed dtseries already created for this subject')
                    end
                end
                
                if exist(temp_name) == 0 % check to see if the file already exists
                    if strcmp(smoothing_kernal,'none')==1 || strcmp(smoothing_kernal,'None')==1 || strcmp(smoothing_kernal,'NONE')==1
                        %cmd = [wb_command output_precision ' -cifti-correlation ' A{i} ' ' A{i} '_' num2str(minutes_limit) '_minutes_of_data_at_FD_' num2str(FD_threshold) '.' suffix2 ' -weights ' B{i} '_' num2str(FD_threshold) '_cifti_censor_FD_vector_' num2str(minutes_limit) '_minutes_of_data_at_' num2str(FD_threshold) '_threshold.txt -fisher-z'];
                        %cmd = [wb_command ' -cifti-correlation ' A{i} ' ' A{i} '_' num2str(minutes_limit) '_minutes_of_data_at_FD_' num2str(FD_threshold) '.' suffix2 ' -weights ' B{i} '_' num2str(FD_threshold) '_cifti_censor_FD_vector_' num2str(minutes_limit) '_minutes_of_data_at_' num2str(FD_threshold) '_threshold.txt -fisher-z'];
                        %tic;
                        %system(cmd);
                        %toc;
                        %clear cmd
                        corr_pt_dt_exaversion(E{i},A{i},ix,temp_name,mask,make_dscalars,wb_command);
                        %disp([temp_name 'created'])
                        clear mask
                        
                    else
                        %cmd = [wb_command output_precision ' -cifti-correlation ' A_smoothed{i} ' ' A_smoothed{i} '_' num2str(minutes_limit) '_minutes_of_data_at_FD_' num2str(FD_threshold) '.' suffix2 ' -weights ' B{i} '_' num2str(FD_threshold) '_cifti_censor_FD_vector_' num2str(minutes_limit) '_minutes_of_data_at_' num2str(FD_threshold) '_threshold.txt -fisher-z'];
                        %cmd = [wb_command ' -cifti-correlation ' A{i} ' ' A{i} '_' num2str(minutes_limit) '_minutes_of_data_at_FD_' num2str(FD_threshold) '.' suffix2 ' -weights ' B{i} '_' num2str(FD_threshold) '_cifti_censor_FD_vector_' num2str(minutes_limit) '_minutes_of_data_at_' num2str(FD_threshold) '_threshold.txt -fisher-z'];
                        %tic;
                        %system(cmd);
                        %toc;
                        %clear cmd
                        corr_pt_dt_exaversion(E{i},A_smoothed{i},ix,temp_name,mask,make_dscalars,wb_command);
                        %disp([temp_name 'created'])
                        clear mask
                        
                    end
                else
                    if strcmp(smoothing_kernal,'none')==1 || strcmp(smoothing_kernal,'None')==1 || strcmp(smoothing_kernal,'NONE')==1
                        subject_conn_exists = [A{i} '_' num2str(minutes_limit) '_minutes_of_data_at_FD_' num2str(FD_threshold) '_ROI' num2str(ix) '.' suffix2 ' already exists']
                    else
                        subject_conn_exists = [A_smoothed{i} '_' num2str(minutes_limit) '_minutes_of_data_at_FD_' num2str(FD_threshold) '_ROI' num2str(ix) '.' suffix2 ' already exists']
                    end
                    
                end
                
            else
                'something is wrong with regard to number of good minutes calc'
            end
        end
    end
    disp('Done running cifti_conn_matrix_to_corr_pt_dt.m')
end
end
