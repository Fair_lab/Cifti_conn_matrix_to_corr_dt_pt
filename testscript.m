addpath('/home/faird/shared/code/internal/utilities/Matlab_CIFTI/') ;
addpath('/home/faird/shared/code/external/utilities/MSCcodebase-master/Utilities/read_write_cifti') ;
addpath('/home/faird/shared/code/external/utilities/MSCcodebase-master/Utilities/read_write_cifti/gifti/') ;

dts='L_55b_ROI/dtseries.conc' ;
pts='L_55b_ROI/ptseries.conc' ;
mot='L_55b_ROI/motion.conc' ;
fd=0.2 ;
tr=2.2 ;
ix=1 ;
min=5.0 ;
smooth='none' ;
lmt='L_55b_ROI/lmidthick.conc' ;
rmt='L_55b_ROI/rmidthick.conc' ;
wbc='/home/faird/shared/code/external/utilities/workbench/1.4.2/workbench/bin_rh_linux64/wb_command' ;
method='mean' ;
comp=7 ;
fisherZ=0 ;
rmoutlier=1 ;
odir='seedmaps';

cifti_conn_matrix_to_corr_pt_dt(dts, pts, mot, fd, tr, ix, min, smooth, ...
    lmt, rmt, wbc, method, comp, fisherZ, rmoutlier, odir)
