function cifti_conn_matrix_to_corr_ROIpt_pt(ROI_ptseries_conc,ix1,pt_series_file_conc,ix2,motionmask_conc,output_name,output_folder,save_ptseries,path_wb_c,use_same_timeseries)

%Robert Hermosillo 11/1/2017

%This code is for correlating an ROI (typically found from statistical analysis) and correlates it with a known region from the ptseries.
%Output is a vector of these correlations (1 per subject).

% Mandatory inputs:
%   ROI_ptseries_conc=a conc file of your ROI time series data. (This is
%   usually build by converting your statstical map into a label file, then
%   applying it to your dense time series.
%   pt_series_file=example: pt_series_file='/group_shares/FAIR_HCP/HCP/processed/ADHD-HumanYouth-OHSU/10050-1/20120605-SIEMENS-Nagel_K-Study/HCP_prerelease_FNL_0_1/MNINonLinear/Results/10050-1_FNL_preproc_Gordon_subcortical.ptseries.nii';
%   motionmask_conc =a vector of frames to keep or discard (1,0).
%   ix1 = Here the ix is meant to represent the index of the parcellation you're trying to correlate your parcellation to.
%   output_name = output name.


%% Load settings
% This code lives originally here: /mnt/max/shared/code/internal/utilities/corr_pt_dt
% remember to add the containing folder to your path: addpath(genpath('/mnt/max/shared/code/internal/utilities/corr_pt_dt'));
%%
%addpath('/mnt/max/shared/code/internal/utilities/corr_pt_dt/support_files');
%path_to_dscalar_template='/mnt/max/shared/data/study/ADHD/HCP/processed/ADHD_NoFMNoT2/10499-1/20150425-SIEMENS_TrioTim-Nagel_K_Study/HCP_release_20161027/10499-1/MNINonLinear/Results/10499-1_FNL_preproc_Atlas_SMOOTHED_2.55.dtseries.nii_all_frames_at_FD_none_ROI342.dscalar.nii.dtseries.nii';
%settings=settings_corr_pt_dt;%
%% Adding paths for this function
%np=size(settings.path,2);
%for i=1:np
%    addpath(genpath(settings.path{i}));
%end

%path_wb_c=settings.path_wb_c; %path to wb_command
if isdeployed ==0
    disp('Adding paths...')
    addpath('/home/faird/shared/code/external/utilities/workbench/1.4.2/workbench/bin_rh_linux64/wb_command');
    addpath('/home/faird/shared/code/external/utilities/gifti-1.6');
    addpath('/home/faird/shared/code/external/utilities/cifti-matlab');
end
reset(RandStream.getGlobalStream,sum(100*clock)); % added to ensure randomness of frame sampling during parellization -RH 05/30/2023

%% Load concatenated paths (i.e. paths to ciftis)

A = importdata(ROI_ptseries_conc);
for i = 1:length(A)
    if exist(A{i}) == 0
        NOTE = ['Subject ROIptseries ' num2str(i) ' does not exist']
        return
    else
    end
end
disp('All ROI ptseries files exist continuing ...')

B = importdata(pt_series_file_conc);
for i = 1:length(B)
    if exist(B{i}) == 0
        NOTE = ['Subject ptseries ' num2str(i) ' does not exist']
        return
    else
    end
end
disp('All ptseries files exist continuing ...')

if motionmask_conc == 1
    disp('no mask used')
else
    C = importdata(motionmask_conc);
    for i = 1:length(C)
        if exist(C{i}) == 0
            NOTE = ['Subject motion censor ' num2str(i) ' does not exist']
            return
        else
        end
    end
    disp('All motion censor files exist continuing ...')
end


for i=1:size(A)
    
    %%Extract ROI from whole ptseries or load custom vector time series.
    if strcmp(B{i}(end-3:end),'.txt') == 1
        seedfile=importdata(B{i});
        [lengthofseedfile widthofseedfile] = size(seedfile);
        if lengthofseedfile >= widthofseedfile
            NOTE = ['Subject ' num2str(i) ' needs to have times series data as a row, instead of a column']
            return
        else
            pt_series_final=seedfile;
        end
        
    elseif strcmp(B{i}(end-3:end),'.csv') == 1
        seedfile=importdata(B{i});
        pt_series_final=seedfile;
    else %ROI assumed to come from Gordon parcellation
        %% Read ptseries
        cii=ciftiopen(B{i},path_wb_c);
        newcii=cii;
        pt_series=double(newcii.cdata);
        N_rois=size(pt_series,1);% count the total rois in this parcellation
        
        %% Error check to make sure ix is a vector and less than or equal to N_rois
        if isempty(ix2)
            ix2=1:N_rois;
        else
            if min(size(ix2,1),size(ix2,2))>2
                error('ix needs to be a vector')
            end
            if sum(ix2>N_rois)>0
                error(['Largest ROI ix must be less than or equal to ' num2str(N_rois)])
            end
        end
        %% Extract the pt_series you would like the correlation to be calculated against your ROI.
        pt_series_final=pt_series(ix2,:); %(i.e.ix 336 corrresponds with the L caudate.
    end
    
    if use_same_timeseries ==1
        ROIpt_series=pt_series(ix1,:);
    else
        %Load single ROI times series.
        if strmatch(A{i}(end-3:end),'.txt') == 1
            seedfile=importdata(A{i});
            [lengthofseedfile widthofseedfile] = size(seedfile);
            if lengthofseedfile >= widthofseedfile
                NOTE = ['Subject ' num2str(i) ' needs to have times series data as a row, instead of a column']
                return
            else
                ROIpt_series=seedfile;
            end
            
        elseif strmatch(A{i}(end-3:end),'.csv') == 1
            seedfile=importdata(A{i});
            ROIpt_series=seedfile;
        else
            %% Read ROI parcellation
            ROIcii=ciftiopen(A{i},path_wb_c);
            ROInewcii=ROIcii;
            ROIpt_series=double(ROInewcii.cdata); %this ROI ptseries only has one parcellation.
            ROIpt_series=ROIpt_series(ix1,:);
        end
    end
    %% Get prepared for motion censoring
    
    if exist('motionmask_conc','var')==0 %check to see if motionmask_conc was supplied as a variable
        n_frames=size(pt_series,2);
        motionmask=ones(n_frames,1)==1;
        
    else
        motionmask_temp=importdata(C{i});
        motionmask=logical(motionmask_temp);
    end
    
    %% calculate correlations
    R=corr(ROIpt_series(:,motionmask)',pt_series_final(:,motionmask)');
    ROI_to_ix_correl(i,1)=R;
    %     if i ==1
    %         all_ROI_ptseries=zeros(362,360); %make an empty array sine these might have differing numbers of frames
    %         all_pt_series_final=zeros(362,360); %make an empty array sine these might have differing numbers of frames
    %     end
    if save_ptseries ==1
        all_ROI_ptseries(i,1:(size(ROIpt_series,2)))=ROIpt_series;
        all_ROI_ptseries_masked(i,:)=ROIpt_series(:,motionmask)';
        all_pt_series_final(i,1:(size(pt_series_final,2)))=pt_series_final;
        all_pt_series_final_masked(i,:)=pt_series_final(:,motionmask)';
    end
    %scatter((ROIpt_series(:,motionmask)'), (pt_series_final(:,motionmask)'))
    disp(i);
    clear motionmask
end

disp('Saving as .mat')
% if strcmp(output_name(end-3:end),'.mat') == 0
%     ext=[output_name '.mat'];
% else
% end

if save_ptseries ==1
    save([output_folder filesep output_name '_ROI_' num2str(ix1) '_to_' num2str(ix2) '_correl.mat'],'A','B','ROI_ptseries_conc', 'pt_series_file_conc', 'all_pt_series_final','all_pt_series_final_masked','all_ROI_ptseries','all_ROI_ptseries_masked','ROI_to_ix_correl');
else
    save([output_folder filesep output_name '_ROI_' num2str(ix1) '_to_' num2str(ix2) '_correl.mat'],'A','B','ROI_ptseries_conc', 'pt_series_file_conc','ROI_to_ix_correl');
end
disp('Done');
