# Seed Map Parcellation Wrapper

## Installation

Clone this repository and save it somewhere on the Linux system that you want to run it from.

## Dependencies

- [Python v3.5.2](https://www.python.org/downloads/releases/python-352) or greater
- MathWorks [MATLAB Runtime Environment (MRE)](https://mathworks.com/products/compiler/matlab-runtime.html) v9.6 (2019a)
- Washington University [Workbench Command (wb_command)](https://github.com/Washington-University/workbench)

## Purpose

Run compiled `cifti_conn_matrix_to_corr_pt_dt` MATLAB script to calculate the correlation between a given set of regions of interest (ROIs) from a parcellated time series to all the timecourses from a dense time series.

## Usage

The `cifti_conn_matrix_to_corr_pt_dt` script can be executed by running `seed_map_wrapper.py`. It requires 4 positional arguments and can take many optional arguments.

### Required Positional Arguments

1. `tr` is one positive floating-point number, the repetition time (time interval between frames of a scan) for your data.
1. `dtseries` is one valid path to a dense timeseries `.conc` file (i.e., a text file with paths to each `.dtseries.nii` file being examined).
1. `ptseries` is one valid path to a parcellated timeseries `.conc` file (i.e., a text file with paths to each `.ptseries.nii` file being examined).
1. `index` takes one or more whole numbers, the index or indices of your region of interest (ROI) in the parcellation. The whole numbers will represent a vector, e.g. `--index 1 2` is the vector [1,2].

The arguments must be included in that order: `tr`, then `dtseries`, then `ptseries`, the `index`. Here is an example of a basic call to this wrapper:
```
python3 seed_map_wrapper.py 5 ./raw/group_dtseries.conc ./raw/group_ptseries.conc 80 81 82
```

For more usage information, call this script with the `--help` command: `python3 seed_map_wrapper.py --help`

### Server-Dependent Arguments

If these arguments are excluded, then by default the wrapper will use hardcoded paths which are only valid on the Rushmore or Exacloud servers. If this script is run on a different server or locally, then these arguments are required. However, if there is already a `wb_command` file in the user's BASH PATH variable, then the script will use that.

`--mre-dir` takes one valid path to the MRE compiler runtime directory. Example: `--mre-dir /usr/local/home/code/Matlab2019aRuntime/v96`

`--wb-command` takes one valid path to an existing workbench command file. Example: `--wb-command /usr/local/home/workbench-1.3.2/bin_64/wb_command`

### Optional Arguments: Run Modes

None of these run mode arguments takes a value.

`--extract-pca` will set the extraction type to `PCA` if it is included. If it is excluded, the extraction type will be `mean`.

`--fisherz` will do a Fisher-Z transformation on the data if the flag is included.

`--mask` removes those frames from the timeseries if the flag is included. Include this flag to keep the frames. Exclude to delete them.

`--remove-outliers` removes outliers from the analysis if the flag is included.

### Optional Arguments: Numerical Values

`--components` takes one whole number, the number of components to be included in the analysis. The default value is 7.

`--fd-threshold` takes one floating-point number, the motion threshold (maximum amount of acceptable motion between frames of a scan) for your data. If this flag is excluded, then the default value of `0.2` will be used. 
              
`--minutes` takes one positive floating-point number, the number of minutes to be used to generate the correlation matrix. If this flag is excluded, then the default minutes limit of `'none'` will make an `allframesbelowFDX` `.dconn`. Subjects will have differing numbers of time points that go into each `.dconn`.

`--smoothing-kernel` takes one floating-point number, the value of the smoothing kernel. This flag lets the user specify smoothing kernel. If this flag is excluded, then the default value of `'none'` will cause no smoothing kernel to be used. Smoothing on `ptseries` is not supported.

### Optional Arguments: File and Directory Paths

`--left` and `--right` each take one valid path, to the subjects' left and right midthickness `.conc` files respectively. Each `.conc` file has only a list of file paths. These arguments are only needed if smoothing will be run. If one of these flags is included without a file path parameter, then the default value of `ADHD_DVARS_group_{left/right}_midthickness_surfaces.conc` will be used. If only a filename is given, then the wrapper will search for the file in the `--input` directory. However, these arguments also accept absolute paths.

`--motion` takes one valid path to a `.conc` file that points to FNL motion `.mat` files for each `dtseries` or `ptseries`. If this flag is included without a file path parameter, then the wrapper will use the default value of `ADHD_DVARS_group_motion.conc`, which contains a list of paths to files called `Power_2014FDOnly.mat`. Exclude this flag to use no motion file.

`--output` takes one valid path to the directory to save all output files in. By default, it will be the present working directory.

`--source` takes one valid path to the directory containing all of the BASH scripts that the wrapper will use to run compiled MATLAB scripts. By default, it will be the present working directory.

### Advanced Usage Example

```
python3 seed_map_wrapper.py 3.5 ./raw/group_dtseries.conc ./raw/group_ptseries.conc 1 2 3 4 5 --mre-dir /home/software/MATLAB/MatlabR2019aRuntime/v96 --fd-threshold 0.5 --left ./left.conc --right ./right.conc --motion ./motion.conc
```

## Meta

This documentation was written by Greg Conan on 2019-09-10 and last updated on 2019-11-22.
