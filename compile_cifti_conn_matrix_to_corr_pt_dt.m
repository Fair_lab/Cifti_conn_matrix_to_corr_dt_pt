 addpath(genpath('/home/faird/shared/code/external/utilities/gifti-1.6/gifti-1.6')) %-RH added 03/02/2021
 addpath(genpath('/home/faird/shared/code/external/utilities/xmltree-2.0')) %-RH added 03/02/2021

%addpath('/mnt/max/shared/code/external/utilities/gifti-1.6/gifti-1.6')
%addpath('/mnt/max/shared/code/external/utilities/xmltree-2.0')


%Example below
%mcc -v -m -d /mnt/max/shared/code/internal/utilities/corr_pt_dt -o cifti_conn_matrix_to_corr_pt_dt_exaversion /mnt/max/shared/code/internal/utilities/corr_pt_dt/cifti_conn_matrix_to_corr_pt_dt_exaversion.m -a /mnt/max/shared/code/external/utilities/Matlab_CIFTI/ -a /mnt/max/shared/code/internal/utilities/hcp_comm_det_damien/isthisanoutlier.m -a /mnt/max/shared/code/internal/utilities/corr_pt_dt/corr_pt_dt_exaversion.m
%mcc -v -m -d /home/faird/shared/code/internal/utilities/seed_map_wrapper -o cifti_conn_matrix_to_corr_pt_dt /home/faird/shared/code/internal/utilities/seed_map_wrapper/cifti_conn_matrix_to_corr_pt_dt.m -a /home/faird/shared/code/internal/utilities/Matlab_CIFTI -a /home/faird/shared/code/internal/utilities/seed_map_wrapper/isthisanoutlier.m -a /home/faird/shared/code/internal/utilities/seed_map_wrapper/corr_pt_dt.m -a /home/faird/shared/code/external/utilities/gifti-1.6/gifti-1.6 -a /home/faird/shared/code/external/utilities/xmltree-2.0 
mcc -v -m -o cifti_conn_matrix_to_corr_pt_dt /home/faird/shared/code/internal/utilities/seed_map_wrapper/cifti_conn_matrix_to_corr_pt_dt.m -a /home/faird/shared/code/internal/utilities/Matlab_CIFTI -a /home/faird/shared/code/internal/utilities/seed_map_wrapper/isthisanoutlier.m -a /home/faird/shared/code/internal/utilities/seed_map_wrapper/corr_pt_dt.m 

%system('/mnt/max/software/MATLAB/R2019a/bin/mcc -v -m -d /mnt/max/shared/code/internal/utilities/corr_pt_dt -o cifti_conn_matrix_to_corr_pt_dt /mnt/max/shared/code/internal/utilities/corr_pt_dt/cifti_conn_matrix_to_corr_pt_dt.m -a /mnt/max/shared/code/external/utilities/Matlab_CIFTI/ -a /mnt/max/shared/code/internal/utilities/hcp_comm_det_damien/isthisanoutlier.m -a /mnt/max/shared/code/internal/utilities/corr_pt_dt/corr_pt_dt.m -a /mnt/max/shared/code/external/utilities/gifti-1.6/gifti-1.6')
