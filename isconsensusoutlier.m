function isconsensusoutlier(dtorpt_series,FDVec)

FD_censored=dtorpt_series(:,(FDvec_keep_idx));
k=1;
for k=1:size(FD_censored,1)
    outlier_matrix(k,:)=isoutlier((FD_censored(k,:)),'median');
end

frame_consensus=sum(outlier_matrix,1);
outlier_threshold=round(size(ROIpt_series,1)*0.75); %See if frame is considered an outlier by more then 75% of greyordinates/parcellations.

o=1;
for o=1:size(frame_consensus,2)
    if frame_consensus(o)>= outlier_threshold
        frameisoutlier(o) = 1;
    else
        frameisoutlier(o) = 0;
    end
end
