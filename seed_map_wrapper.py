#! /usr/bin/env python3

"""
Seed map wrapper
Greg Conan: gconan@umn.edu
Created 2019-07-05
Updated 2021-02-12
"""
""""
Updated 2023-12-05
Vanessa Morgan: morg0448@umn.edu
Update reason: MRE and Workbench paths are out of date
"""

##################################
#
# Wrapper for the cifti_conn_matrix_to_corr_pt_dt MATLAB script
#
##################################

import argparse
from datetime import datetime
import os
from socket import gethostname
import subprocess
import sys

# Constants


# Default directories for input data, wrapped scripts (src), and output data
DEFAULT_FD_THRESHOLD = "0.2"
DEFAULT_NUM_COMPONENTS = "7"
DEFAULT_OUTPUT_DIR = os.path.abspath(os.getcwd())
DEFAULT_SOURCE = os.path.dirname(os.path.abspath(sys.argv[0]))

# Default names of data files used by wrapped scripts
GROUP_LEFT_CONC = "ADHD_DVARS_group_left_midthickness_surfaces.conc"
GROUP_MOTION_CONC = "ADHD_DVARS_group_motion.conc"
GROUP_RIGHT_CONC = "ADHD_DVARS_group_right_midthickness_surfaces.conc"

# MATLAB Runtime Environment (MRE) directories which depend on the host server
#MRE_EXA = ("/home/exacloud/lustre1/fnl_lab/code/external/utilities/"
           #"Matlab2019aRuntime/v96")
#MRE_RUSH = "/mnt/max/software/MATLAB/R2019a"
MRE_EXA = ("/home/faird/shared/code/external/utilities/MATLAB_Runtime_R2019a/") #Need to change this
MRE_RUSH = "/mnt/max/software/MATLAB/R2019a"

# Name of script to call
SCRIPT_TO_RUN = "cifti_conn_matrix_to_corr_pt_dt"

# Workbench commands which depend on the host server
#WB_EXA = ("/home/exacloud/lustre1/fnl_lab/code/external/utilities/"
 #         "workbench-1.3.2/bin_rh_linux64/wb_command")
WB_RUSH = "/mnt/max/software/workbench/bin_linux64/wb_command"

WB_EXA = ("/home/feczk001/shared/code/external/utilities/workbench/1.4.2/workbench/bin_rh_linux64/wb_command")


def main():

    # Get all command line arguments from user
    cli_args = get_cli_args()

    def timestamp(completion, script):
        """
        :param completion: String which is either "Started" or "Finished"
        :param script: String which is the name of this script
        :return: String with the current date and time in a readable format
        """
        return "{} running {} on {}".format(
            completion, script, datetime.now().strftime("%Y %b %d at %H:%M:%S")
        )

    # Get, print, and store the date and time when this script started running
    started_at = timestamp("Started", sys.argv[0])
    print(started_at)

    # Run seed map parcellation and print timestamps
    globals()[SCRIPT_TO_RUN](cli_args)
    print(started_at)
    print(timestamp("Finished", sys.argv[0]))


def get_cli_args():
    """
    Get and validate all args from command line using argparse.
    :return: Namespace containing all validated inputted command line arguments
    """
    # Default messages used by multiple parameters' help message descriptions
    msg_valid_dir = "This argument must be a valid path to an existing folder."
    default_flag_msg = (" If this flag is included but no file path is "
                        "given, then {} will be used.")
    default_msg = " By default, this argument's value will be '{}'."
    default_svr_msg = default_msg + (" If that says 'None', then you must "
                                     "include this argument.")

    # Default server-dependent argument values
    server_args = {arg: get_default_server_arg(arg)
                   for arg in ("mre_dir", "wb_command")}

    # Create arg parser
    parser = argparse.ArgumentParser(
        description="Wrapper to generate connectivity matrices."
    )

    # Positional arguments: Repetition time of scan and time series filenames
    parser.add_argument(
        "tr",
        type=none_or_valid_float_value_as_string,
        help=("Specify the repetition time (time interval between frames of a "
              "scan) for your data.")
    )
    parser.add_argument(
        "dtseries",
        type=readable_file,
        help=("Name of dense timeseries .conc file (i.e. text "
              "file with paths to each file being examined).")
    )
    parser.add_argument(
        "ptseries",
        type=readable_file,
        help=("Name of parcellated timeseries .conc file (i.e. text "
              "file with paths to each file being examined).")
    )
    parser.add_argument(
        "index",
        nargs="*",
        type=int,
        help=("Index/indicess of your region of interest (ROI) in the "
              "parcellation. This should either be one whole number or "
              "several whole numbers representing a vector, e.g. --index 1 2 "
              "is the vector [1, 2].")
    )

    # Optional: Specify mask
    parser.add_argument(
        "-a",
        "--mask",
        default="0",
        action="store_const",
        const="1",
        help=("Option to use mask. If provided, removes those frames from the "
              "timeseries. Include this flag to keep the frames. Exclude to "
              "delete them.")
    )

    # Optional: Specify number of components
    parser.add_argument(
        "-c",
        "--components",
        type=whole_number_as_string,
        default=DEFAULT_NUM_COMPONENTS,
        help=("Number of components."
              + default_msg.format(DEFAULT_NUM_COMPONENTS)) 
    )

    # Optional: Extract mean or PCA
    parser.add_argument(
        "-e",
        "--extract-pca",
        default="mean",
        action="store_const",
        const="pca",
        help=("Include this flag to set the extraction type to 'PCA'. If it "
              "is excluded, the extraction type will be 'mean'.")
    )

    # Optional: Get frame displacement motion threshold
    parser.add_argument(
        "-fd",
        "--fd-threshold",
        default=DEFAULT_FD_THRESHOLD,
        type=none_or_valid_float_value_as_string,
        help=("Specify motion threshold (maximum amount of acceptable motion "
              "between frames of a scan) for your data."
              + default_msg.format(DEFAULT_FD_THRESHOLD))
    )

    # Optional: Get name of left surface .conc file
    parser.add_argument(
        "-l",
        "--left",
        type=readable_file_or_none,
        nargs="?",
        const=GROUP_LEFT_CONC,
        default="none",
        help=(".conc file path of subjects' left midthicknessfile. Include "
              "this flag if you will run smoothing." +
              default_flag_msg.format(GROUP_LEFT_CONC))
    )

    # Optional: Get name of .conc file listing FNL motion mat files
    parser.add_argument(
        "-m",
        "--motion",
        type=readable_file_or_none,
        nargs="?",
        default="none",
        const=GROUP_MOTION_CONC,
        help=("Path to .conc file that points to FNL motion mat files for "
              "each dt or ptseries. By default, this wrapper will do no "
              "motion correction." + default_flag_msg.format(GROUP_MOTION_CONC))
    )

    # Optional: Give path to MATLAB Runtime Environment (MRE) directory
    parser.add_argument(
        "-mre",
        "--mre-dir",
        type=readable_dir,
        default=server_args["mre_dir"],
        help=("Path to directory containing MATLAB Runtime Environment (MRE)"
              "version 9.6. This is used to run compiled MATLAB scripts. "
              + msg_valid_dir + default_svr_msg.format(server_args["mre_dir"]))
    )

    # Optional: Get minutes limit
    parser.add_argument(
        "-min",
        "--minutes",
        default="none",
        type=none_or_valid_float_value_as_string,
        help=("Specify the number of minutes to be used to generate the "
              "correlation matrix. The default minutes limit of 'none' "
              "will make an 'allframesbelowFDX' .dconn. Subjects will have "
              "differing numbers of time points that go into each .dconn")
    )

    # Optional: Specify output file or folder depending on script_to_run
    parser.add_argument(
        "-out",
        "--output",
        default=DEFAULT_OUTPUT_DIR,
        help=("--output should be the directory to save all output files in. "
              + default_msg.format(DEFAULT_OUTPUT_DIR))
    )

    # Optional: Get name of right surface .conc file
    parser.add_argument(
        "-r",
        "--right",
        type=readable_file_or_none,
        nargs="?",
        const=GROUP_RIGHT_CONC,
        default="none",
        help=(".conc file path of subjects' right midthicknessfile. Include "
              "this flag if smoothing will be run."
              + default_flag_msg.format(GROUP_RIGHT_CONC))
    )

    # Optional: Remove outliers
    parser.add_argument(
        "-outliers",
        "--remove-outliers",
        default="0",
        action="store_const",
        const="1",
        help="Include this flag to remove outliers."
    )

    # Optional: Specify the source directory where the run scripts are
    parser.add_argument(
        "-s",
        "--source",
        default=DEFAULT_SOURCE,
        help=("Folder with the BASH scripts to run compiled MATLAB script. "
              + default_msg.format(DEFAULT_SOURCE))
    )

    # Optional: Get smoothing kernel for dconns
    parser.add_argument(
        "-smooth",
        "--smoothing_kernel",
        default="none",
        type=none_or_valid_float_value_as_string,
        help=("Specify smoothing kernel. If this flag is excluded, then no "
              "smoothing kernel will be used. Smoothing on ptseries is not "
              "supported.")
    )

    # Optional: Specify path to wb_command
    parser.add_argument(
        "-wb",
        "--wb_command",
        default=server_args["wb_command"],
        type=readable_file,
        help=("Path to workbench command file."
              + default_svr_msg.format(server_args["wb_command"]))
    )

    # Optional: Do Fisher-Z transformation
    parser.add_argument(
        "-z",
        "--fisherz",
        default="0",
        action="store_const",
        const="1",
        help="Include this flag to do a Fisher Z-transformation on the data."
    )

    return validate_cli_args(parser.parse_args(), parser)


def whole_number_as_string(str_to_check):
    """
    Throws ArgumentTypeError unless str_to_check is a string representing a
    positive integer.
    :param str_to_check: string to validate
    :return: string containing only digits
    """
    try:
        assert str_to_check.isdigit()
        return str_to_check
    except AssertionError:
        raise argparse.ArgumentTypeError("Argument must be whole number(s)")


def readable_file(path):
    """
    Throw argparse exception unless parameter is a valid readable filename 
    string. This is used instead of argparse.FileType("r") because the latter 
    leaves an open file handle, which has caused problems.
    :param path: Parameter to check if it represents a valid filename
    :return: String representing a valid filename
    """
    try:
        assert os.access(path, os.R_OK)
        return os.path.abspath(path)
    except (AssertionError, OSError, TypeError):
        raise argparse.ArgumentTypeError("Cannot read file at {}".format(path))


def readable_file_or_none(path):
    """
    Throw argparse exception unless path either is "none" or points to a 
    readable file.
    :param path: Parameter to check if it represents a valid filename
    :return: String representing a valid filename, or the string "none"
    """
    return path if path == "none" else readable_file(path)


def readable_dir(path):
    """
    Throw argparse exception unless path is a string representing a valid path 
    to a readable directory.
    :param path: Parameter to check if it represents a valid filename
    :return: String representing a valid path to an existing readable directory
    """
    if os.path.isdir(path):
        return readable_file(path)
    else:
        raise argparse.ArgumentTypeError(("{} is not a readable "
                                          "directory").format(path))


def none_or_valid_float_value_as_string(str_to_check):
    """
    Unless a string is "none", tries to convert it to a float and back to check
    that it represents a valid float value. Throws ValueError if type
    conversion fails. This function is only needed because the MATLAB scripts
    take some arguments either as a float value in string form or as "none".
    :param str_to_check: string to validate
    :return: string which is either "none" or represents a valid float value
    """
    return str_to_check if str_to_check == "none" else str(float(str_to_check))


def validate_cli_args(cli_args, parser):
    """
    Check that all command line arguments will allow this script to work.
    :param cli_args: argparse namespace with all command-line arguments
    :param parser: argparse ArgumentParser to raise error if anything's invalid
    :return: Validated command-line arguments argparse namespace
    """
    # Check that BASH script to run compiled MATLAB scripts are executable
    if not os.access(os.path.join(cli_args.source,
                                  "run_{}.sh".format(SCRIPT_TO_RUN)), os.X_OK):
        parser.error("Cannot execute " + SCRIPT_TO_RUN)

    # Try to find valid output directory and make one if none is found; also
    # make sure that index is a string representing a list of whole numbers
    for arg in ("output", "index"):
        setattr(cli_args, arg, globals()["validate_" + arg]
                (getattr(cli_args, arg), parser))

    return cli_args


def validate_output(output, parser):
    """
    Validate that output directory exists and is writeable, or create one, and
    then return valid path to that output directory without trailing slash
    :param output: File path of output directory to validate
    :param parser: argparse ArgumentParser to raise error if path is invalid
    :return: Valid path to output directory
    """
    try:
        output = os.path.abspath(output)
        if not os.path.isdir(output):
            os.makedirs(output)
        assert os.access(output, os.W_OK)
        return output[:-1] if output[-1] == "/" else output
    except (OSError, TypeError):
        parser.error("Cannot make output folder at " + output)
    except AssertionError:
        parser.error("Cannot write to output folder at " + output)


def validate_index(ix, parser):
    """
    :param ix: List of integers
    :return: String representing a list of whole numbers without whitespaces
    """
    for i in ix:
        if i < 0:
            parser.error("ROI index values must be whole numbers")
    return str(ix).replace(" ", "")


def get_default_server_arg(argname):
    """
    :param argname: String, "wb_command" or "mre_dir"
    :return: String, a default wb_command or mre_dir path argument if a valid
             one is found; otherwise None
    """ 
    # Local variables: Server host name, arg names, default paths to args on
    # specific OHSU servers, and return value
    host = gethostname().lower()
    if "cn" in host: #tk, fix later
        host = "exacloud"
    args = ("mre_dir", "wb_command")
    server_default_paths = {"rushmore": {args[0]: MRE_RUSH, args[1]: WB_RUSH},
                            "exacloud": {args[0]: MRE_EXA, args[1]: WB_EXA}}
    default_arg = None

    # For mre_dir and wb_command, set them to defaults if the user did not give
    # specific paths and if the script is being run on a known server
    if host in server_default_paths:
        default_arg = server_default_paths[host][argname]
    elif argname == args[1]:
        try:  # If wb_command is already in BASH PATH, get & format it
            default_arg = subprocess.check_output(("which", argname)
                                                  ).decode("utf-8").strip()
        except subprocess.CalledProcessError:
            pass
    return default_arg


def set_obj_attr_if_none(obj, arg, new_value):
    """
    :param arg: String which is the name of the attribute to check
    :param obj: Object that has its attributes checked/modified
    :param new_value: New value of obj's arg attribute if obj did not have it
    :return: obj, but guaranteed to have a value for its arg attribute
    """
    if not getattr(obj, arg, None):
       setattr(obj, arg, new_value)
    return obj


def cifti_conn_matrix_to_corr_pt_dt(cli_args):
    """
    Call compiled cifti_conn_matrix_to_corr_pt_dt MATLAB script
    :param cli_args: argparse namespace with all command-line arguments
    :return: N/A
    """

    p = subprocess.check_call((
        os.path.join(cli_args.source, "run_{}.sh".format(SCRIPT_TO_RUN)),
        cli_args.mre_dir,
        cli_args.dtseries,
        cli_args.ptseries,
        cli_args.motion,
        cli_args.fd_threshold,
        cli_args.tr,
        cli_args.index,
        cli_args.minutes,
        cli_args.smoothing_kernel,
        cli_args.left,
        cli_args.right,
        cli_args.wb_command,
        cli_args.extract_pca,
        cli_args.components,
        cli_args.fisherz,
        cli_args.remove_outliers,
        cli_args.output
    ))



if __name__ == '__main__':
    main()
