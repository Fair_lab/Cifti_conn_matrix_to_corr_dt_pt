function corr_pt_dt(pt_series_file,dt_series_file,ix,output_file,mask,make_dscalars,wb_command)

%% corr_pt_dt(pt_series_file,dt_series_file,ix,output_file)
%% Oscar Miranda-Dominguez, OCt 13, 2016

% This function calculate the correlation between a given set of ROIs from
% a parcellated time series to all the timecourses from a dense time series
%
% The output is a dtseries file
%
% Mandatory inputs:
%   pt_series_file, 
%       example: pt_series_file='/group_shares/FAIR_HCP/HCP/processed/ADHD-HumanYouth-OHSU/10050-1/20120605-SIEMENS-Nagel_K-Study/HCP_prerelease_FNL_0_1/MNINonLinear/Results/10050-1_FNL_preproc_Gordon_subcortical.ptseries.nii';     
%
%   dt_series_file, 
%       example: dt_series_file='/group_shares/FAIR_HCP/HCP/processed/ADHD-HumanYouth-OHSU/10050-1/20120605-SIEMENS-Nagel_K-Study/HCP_prerelease_FNL_0_1/MNINonLinear/Results/10050-1_FNL_preproc_Atlas.dtseries.nii';
%
% Optional inputs:
%
%   ix, indices of the ROIS in the dt_series. This is optional. If not
%   provided, correlation will be calculated using as seed all the ROIs
%   from the ptseries. If provided, It needs to be a vector,
%       example: ix=[1 2];
%
%   output_file, Optional, If provided, it should have an existing path and filename
%   (without the extension dtseries.nii). If not provided, is going to take
%   the path from the ptseries and the filename will be
%   corr_pt_dt.dtseries.nii
%
%  mask, Optional. If provided, removes those frames from the timeseries.
%  One means keep, 0 means delete

%% Robert Hermosillo, Oct 5, 2017
%  added an option to add save the correlations as a dscalar rather than a
%  dtseries. Add addtional variable to make_dscalars.

%% Load settings
% This code lives originally here: /group_shares/PSYCH/code/development/utilities/corr_pt_dt
% remeber to add the containing folder to your path: addpath(genpath('/group_shares/PSYCH/code/development/utilities/corr_pt_dt'));
%%
% addpath('/mnt/max/shared/code/internal/utilities/corr_pt_dt/support_files');
 path_to_dscalar_template='/home/exacloud/lustre1/fnl_lab/data/HCP/processed/ADHD_NoFMNoT2/10499-1/20150425-SIEMENS_TrioTim-Nagel_K_Study/HCP_release_20161027/10499-1/MNINonLinear/Results/10499-1_FNL_preproc_Atlas_SMOOTHED_2.55.dtseries.nii_all_frames_at_FD_none_ROI342.dscalar.nii.dtseries.nii';
% settings=settings_corr_pt_dt;%
% %% Adding paths for this function
% np=size(settings.path,2);
% for i=1:np
%     addpath(genpath(settings.path{i}));
% end
% 
% path_wb_c=settings.path_wb_c; %path to wb_command
%path_wb_c='/home/exacloud/lustre1/fnl_lab/code/external/utilities/workbench-9253ac2/bin_rh_linux64/wb_command'; %path to wb_command
path_wb_c=wb_command;
%% Make default name for output file, if not provided
if nargin<4
    output_file=[];
end
if isempty(output_file)
    [pathstr_dt,name_dt,ext_dt] = fileparts(dt_series_file);
    % pathstr_out=pathstr_pt;
    output_file=[pathstr_dt filesep 'corr_pt_dt'];
end


% if or(nargin<4,isempty(output_file))
% % [pathstr_pt,name_pt,ext_pt] = fileparts(pt_series_file);
% [pathstr_dt,name_dt,ext_dt] = fileparts(dt_series_file);
% % pathstr_out=pathstr_pt;
% output_file=[pathstr_dt filesep 'corr_pt_dt'];
% end

%% Read ptseries
if ischar(pt_series_file)==1
   pt_series_file=cellstr(pt_series_file);
else
end

i=1;
if strmatch(pt_series_file{i}(end-3:end),'.txt') == 1
    seedfile=importdata(pt_series_file{i});
    [lengthofseedfile widthofseedfile] = size(seedfile);
    if lengthofseedfile >= widthofseedfile
        NOTE = ['Subject ' num2str(i) ' needs to have times series data as a row, instead of a column']
        return
    else
        pt_series_final=seedfile;
    end
    
elseif strmatch(pt_series_file{i}(end-3:end),'.csv') == 1
    seedfile=importdata(pt_series_file{i});
    if lengthofseedfile >= widthofseedfile
        NOTE = ['Subject ' num2str(i) ' needs to have times series data as a row, instead of a column']
        return
    else
        pt_series_final=seedfile;
    end
else %read ptseries file %usually Gordon_subcortical
    disp(pt_series_file{i});
    cii=ciftiopen(pt_series_file{i},path_wb_c);
    newcii=cii;
    pt_series=double(newcii.cdata);
    N_rois=size(pt_series,1);% count the total rois in this parcellation
    
    
    %% Error check to make sure ix is a vector and less than or equal to N_rois
    if isempty(ix)
        ix=1:n_rois;
    else
        if min(size(ix,1),size(ix,2))>2
            error('ix needs to be a vector')
        end
        if sum(ix>N_rois)>0
            error(['Largest ROI ix must be less than or equal to ' num2str(N_rois)])
        end
    end
    %% Extract the pt_series you like the correlation to be calculated
    pt_series_final=pt_series(ix,:);
    
end

%% Read dtseries
cii=ciftiopen(dt_series_file,path_wb_c);
newcii=cii;
dt_series=double(newcii.cdata);
%% Get prepared for motion censoring

if nargin<5
    n_frames=size(dt_series,2);
    mask=ones(n_frames,1)==1;
end

%% calculate correlations
R=corr(pt_series_final(:,mask)',dt_series(:,mask)');

if nargin==6 &&  make_dscalars==1%save as dscalar
   cii_dscalar=ciftiopen(path_to_dscalar_template,path_wb_c);
  newcii=cii_dscalar;  
    newcii.cdata=R';
    %% Save data as cifti
    %ciftisave(newcii,output_file,path_wb_c); % Making your cifti
    % modified from save to include reset-scalars flag.
    tic
    save(newcii,[output_file '.gii'],'ExternalFileBinary')
    toc
    
    tic
    unix([path_wb_c ' -cifti-convert -from-gifti-ext ' output_file '.gii ' output_file ' -reset-scalars' ]);
    toc
    
    unix([' /bin/rm ' output_file '.gii ' output_file '.dat ']);

    disp(['Done, file ' output_file '  saved']);
    
else %save as dtseries
    
    newcii.cdata=R';
    %% Save data as cifti
    output_file=[output_file '.dtseries.nii'];
    %output_file=[output_file];
    ciftisave(newcii,output_file,path_wb_c); % Making your cifti
    disp(['Done, file ' output_file '  saved']);
end

%% Validation
% temp=randi(3);
% ix1=ix(temp);
% ix2=randi(91282);
% R2=corr(pt_series(ix1,:)',dt_series(ix2,:)');
% [R2 R(temp,ix2)]
