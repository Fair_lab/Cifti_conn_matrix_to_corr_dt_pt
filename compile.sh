#!/bin/bash

module load matlab/R2019a

/common/software/install/migrated/matlab/R2019a/bin/mcc -v -m \
    -o cifti_conn_matrix_to_corr_pt_dt          \
       cifti_conn_matrix_to_corr_pt_dt.m        \
    -a isthisanoutlier.m                        \
    -a /home/faird/shared/code/internal/utilities/Matlab_CIFTI/*.m \
    -a /home/faird/shared/code/external/utilities/MSCcodebase-master/Utilities/read_write_cifti/fileio/ \
    -a /home/faird/shared/code/external/utilities/MSCcodebase-master/Utilities/read_write_cifti/gifti/@xmltree/private/ \
    -a  /home/faird/shared/code/external/utilities/MSCcodebase-master/Utilities/read_write_cifti/gifti/@gifti/private/
